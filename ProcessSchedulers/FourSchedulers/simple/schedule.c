#include "schedule.h"
#include <stdlib.h>
#include <stdio.h>

struct node {
	int value;
	struct node* next;
};

struct node* root;

/**
 * Function to initialize any global variables for the scheduler.
 */
void init(){
	root = NULL;
}

/**
 * Function to add a process to the scheduler
 * @Param pid - the ID for the process/thread to be added to the
 *      scheduler queue
 * @return true/false response for if the addition was successful
 */
int addProcess(int pid){
	struct node* cur = NULL;
		if(root == NULL)
		{
	  	root = (struct node*) malloc( sizeof(struct node));
			cur = root;
			root -> value = pid;
			root -> next = NULL;
		}
		else
		{
			cur = root;
			while(cur->next)
			{
				cur = cur -> next;
			}
			cur -> next = (struct node*) malloc( sizeof(struct node));
			cur -> next -> value = pid;
			cur -> next -> next = NULL;
		}
		if(root != NULL)
			return 1;
    return 0;
}

/**
 * Function to get the next process from the scheduler
 *
 * @Return returns the thread id of the next process that should be
 *      executed, returns -1 if there are no processes
 */
int nextProcess(){
	if (root == NULL)
		return -1;
	else
	{
		struct node* cur;
		cur = root;
		int x = root -> value;
		root = root -> next;
		free(cur);
	  return x;
	}
}

/**
 * Function that returns a boolean 1 True/0 False based on if there are any
 * processes still scheduled
 * @Return 1 if there are processes still scheduled 0 if there are no more
 *		scheduled processes
 */
int hasProcess(){
 if(root == NULL)
 		return 0;
	else
		return 1;
}
