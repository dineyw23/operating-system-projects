#include "schedule.h"
#include <stdlib.h>

struct node {
	int value;
	struct node* next;
	struct node* prev;
};

struct node* root[4];
int z;
/**
 * Function to initialize any global variables for the scheduler.
 */
void init(){
	z = 0;
	int i = 0;
	for(i = 0; i < 4; i++)
	{
		root[i] = NULL;
	}
}

/**
 * Function to add a process to the scheduler
 * @Param pid - the ID for the process/thread to be added to the
 *      scheduler queue
 * @Param priority - priority of the process being added
 * @return true/false response for if the addition was successful
 */
int addProcess(int pid, int priority){

	priority--; // Just to match indices
	//printf("%d\n",priority);
	if(root[priority] == NULL)
	{
		root[priority] = (struct node*) malloc(sizeof(struct node));
		root[priority] -> value = pid;
		root[priority] -> next = NULL;
		root[priority] -> prev = NULL;
	}
	else
	{
		struct node* cur = NULL;
		cur = root[priority];
		while (cur -> next)
		{
				cur = cur -> next;
		}
		struct node* temp;
		temp = cur;
    cur -> next = (struct node*) malloc(sizeof(struct node));
    cur -> next -> value = pid;
    cur -> next -> next = NULL;
		cur -> next -> prev = temp;
	}
	if(root != NULL)
		return 1;
	return 0;
}

/**
 * Function to remove a process from the scheduler queue
 * @Param pid - the ID for the process/thread to be removed from the
 *      scheduler queue
 * @Return true/false response for if the removal was successful
 */
int removeProcess(int pid){
	struct node* cur = NULL;
	struct node* temp = NULL;
	int i = 0;
	for(i = 0; i < 4; i++) //Four levels
	{
		if(root[i] != NULL && root[i] -> value == pid)
		{
			cur = root[i];
			root[i] = root[i] -> next;
			free(cur);
			return 1;
		}
		else if(root[i] != NULL)
		{
			temp = root[i];					//Not root[i]
			cur = temp -> next;		//Hence start from second
			while(cur != NULL)
			{
				if(cur -> value != pid)
				{
					temp = cur;
					cur = cur -> next;
				}
				else if(cur != NULL && cur -> value == pid)
				{
					temp -> next = cur -> next;
					temp -> prev = cur -> prev;
					free(cur);
					return 1;
				}
			}
		}
	}
	return 0;
}
/**
 * Function to get the next process from the scheduler
 * @Param time - pass by pointer variable to store the quanta of time
 * 		the scheduled process should run for
 * @Return returns the thread id of the next process that should be
 *      executed, returns -1 if there are no processes
 */

int nextProcess(int *time){
	int i = 0;
	if(root[z] == NULL){
		z = z + 1;
			if(root[z] == NULL)
			{
					z = z + 1;
				if(root[z] == NULL)
				{
					z = z + 1;
					if(root[z] == NULL)
					{
						  z = 0;
							return -1;
					}
					else
					{
						i = z;
						if(z != 3)
						 z++;
						else
							z = 0;
					}
				}
				else
				{
					i = z;
					if(z != 3)
					 z++;
					else
						z = 0;
				}
			}
			else
			{
				i = z;
				if(z != 3)
				 z++;
				else
					z = 0;
			}
	}
	else
	{
	  i = z;
		if(z != 3)
		 z++;
		else
			z = 0;
	}
	 *time = 4 - i; // Complimentary to priority level for level 1 = 4 quanta
	  //level 1 (4 times)= 4 - 0;
		//level 2 (3 times)= 4 - 1;
		//level 3 (2 times) = 4 - 2;
		//level 4 (1 time) = 4 - 3;
	 	int x = root[i] -> value;
		removeProcess(x);
		addProcess(x,i + 1); // +1 to match priority level, decrement in add
		return x;

	return -1; //Never
}

/**
 * Function that returns a boolean 1 True/0 False based on if there are any
 * processes still scheduled
 * @Return 1 if there are processes still scheduled 0 if there are no more
 *		scheduled processes
 */
int hasProcess(){
	int i = 0;
	for(i = 0; i < 4; i++)
	{
		if(root[i] != NULL)
			return 1;
	}
	return 0;
}
