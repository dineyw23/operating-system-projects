#include "schedule.h"
#include <stdlib.h>
#include <stdio.h>

struct node {
	int value;
	int age;
	struct node* next;
	struct node* prev;
};
//Three levels
struct node* root[3];
int z;

/**
 * Function to initialize any global variables for the scheduler.
 */
void init(){
	int i = 0;
  z = 0;
	for(i = 0;i < 3; i++)
		root[i] = NULL;
}
/**
 * Function called every simulated time period to provide a mechanism
 * to age the scheduled processes and trigger feedback if needed.
 */
void age(){
	int i = 1;	//0 level we can skip. Can't increase the priority
	if(root[i] == NULL){
		i = (i + 1) % 3;
			if(root[i] == NULL)
				return;
	}
	struct node* cur = NULL;
	for(;i < 3; i++) //i is risky -> Like for in sim.c
	{
		if(root[i] != NULL)
			cur = root[i];
		while(cur)
		{ //0 till 1000 is okay.
			if(cur -> age < 1000 || cur -> age == 1000)
				cur -> age += 1;	//age should be incremented, everytime
				if(cur -> age > 1000)
				{
					int local = cur -> value;
					removeProcess(cur->value);
					addProcess(local, i - 1); //F/b to higher priority
				}
				cur = cur -> next;
		 }
	 }
 }
/*
 * Function to add a process to the scheduler
 * @Param pid - the ID for the process/thread to be added to the
 *      scheduler queue
 * @Param priority - priority of the process being added
 * @return true/false response for if the addition was successful
 */
int addProcess(int pid, int priority)
{
	if(root[priority] == NULL)
	{
		root[priority] = (struct node*) malloc(sizeof(struct node));
		root[priority] -> value = pid;
		root[priority] -> age = 0;
		root[priority] -> next = NULL;
		root[priority] -> prev = NULL;
		return 1;
	}
	else
	{
		struct node* cur = NULL;
		cur = root[priority];
		while (cur -> next)
		{
				cur = cur -> next;
		}
				struct node* x;
				x  = cur;
        cur -> next = (struct node*) malloc(sizeof(struct node));
				cur -> next -> age = 0;
        cur -> next -> value = pid;
				cur -> next -> next = NULL;
		 		cur -> next -> prev = x;
				return 1;
    }
	return 0;
}

/**
 * Function to remove a process from the scheduler queue
 * @Param pid - the ID for the process/thread to be removed from the
 *      scheduler queue
 * @Return true/false response for if the removal was successful
 */
int removeProcess(int pid){

	struct node* cur = NULL;
	struct node* temp = NULL;
	int i = 0; // Same again
	for(i = 0; i < 3; i++)
	{
		if(root[i] != NULL && root[i] -> value == pid)
		{
			cur = root[i];
			root[i] = root[i] -> next;
			free(cur);
			return 1;
		}
		else if(root[i] != NULL)
		{
			temp = root[i];					//Not root[i]
			cur = temp -> next;	  	//Hence start from second
			while(cur != NULL)
			{
				if(cur -> value != pid)
				{
					temp = cur;
					cur = cur -> next;
				}
				else if(cur != NULL && cur -> value == pid )//&& cur -> next != NULL)
				{
					temp -> next = cur -> next;
					temp -> prev = cur -> prev;
					free(cur);
					return 1;
				}
			}
		}
	}
  return 0;
}
/**
 * Function to get the next process from the scheduler
 * @Param time - pass by pointer variable to store the quanta of time
 * 		the scheduled process should run for, returns -1 if should run to completion.
 * @Return returns the thread id of the next process that should be
 *      executed, returns -1 if there are no processes
 */

int nextProcess(int *time){
	if(root[z] == NULL){
		 z++;	 //No nodes in priority 0
		if(root[z] == NULL)
			z++;  //No nodes in priority 0 & 1
			if(root[z] == NULL)
			{  //No nodes in priority 0,1 & 2
			  	z = 0; // everytime from top
					return -1;
			}
  }
	int	temp = z;
	z = 0; // Everytime, start scanning from top
	if(temp == 1)
	{ //Same as multilevelRR
		*time = 4;
		int x = root[temp] -> value;
		removeProcess(x);
		addProcess(x,temp);
		return x;
	}
	else if (temp == 2) // Least priority
	{  //Same as multilevelRR
		*time = 1;
		int x = root[temp] -> value;
		removeProcess(x);
		addProcess(x,temp);
		return x;
	}
  else if(temp == 0)  //FCFS, easy highest priority
	{
		*time = -1;
		struct node* cur = root[temp];
		int x = root[temp] -> value;
		root[temp] = root[temp] -> next;
		removeProcess(cur -> value);
		return x;
	}
  return -1; //Never
}

/**
 * Function that returns a boolean 1 True/0 False based on if there are any
 * processes still scheduled
 * @Return 1 if there are processes still scheduled 0 if there are no more
 *		scheduled processes
 */
int hasProcess(){
	int i = 0;
	for(i = 0; i < 3; i++)
	{
		if(root[i] != NULL)
			return 1;
	}
	return 0;
}
