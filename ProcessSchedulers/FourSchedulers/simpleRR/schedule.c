#include "schedule.h"
#include <stdlib.h>
#include <stdio.h>

struct node {
	int value;
	struct node* prev;
	struct node* next;
};
struct node* root;
/**
 * Function to initialize any global variables for the scheduler.
 */
void init(){
	root = NULL;
}

/**
 * Function to add a process to the scheduler
 * @Param pid - the ID for the process/thread to be added to the
 *      scheduler queue
 * @return true/false response for if the addition was successful
 */
int addProcess(int pid){

	if(root == NULL)
	{
		  root = (struct node*) malloc( sizeof(struct node));
			root -> value = pid;
	  	root -> next = NULL;
			root -> prev = NULL;
	}
	else
	{
		struct node* cur = NULL;
		cur = root;
		while(cur -> next)
		{
			cur = cur -> next;
		}
		struct node* temp;
		temp = cur;
		cur -> next = (struct node*) malloc(sizeof(struct node));
		cur -> next -> value = pid;
		cur -> next -> next = NULL;
		cur -> next -> prev = temp;
	}
	if(root != NULL)
		return 1;

	return 0;
}

/**
 * Function to remove a process from the scheduler queue
 * @Param pid - the ID for the process/thread to be removed from the
 *      scheduler queue
 * @Return true/false response for if the removal was successful
 */
int removeProcess(int pid){
	struct node* cur = NULL;
	struct node* temp = NULL;

	if(hasProcess() && root -> value == pid)
	{
		cur = root;
		root = root -> next;
		free(cur);
		return 1;
	}
	else if(root != NULL)
	{
		temp = root;					//Not root
		cur = temp -> next;		//Hence start from second
		while(cur != NULL)
		{
			if(cur -> value != pid)
			{
				temp = cur;
				cur = cur -> next;
			}
			else if(cur != NULL && cur -> value == pid)
			{
				temp -> next = cur -> next;
				temp -> prev = cur -> prev;
				free(cur);
				return 1;
			}
		}
	}
	return 0;
}
/**
 * Function to get the next process from the scheduler
 * @Param time - pass by pointer variable to store the quanta of time
 * 		the scheduled process should run for
 * @Return returns the thread id of the next process that should be
 *      executed, returns -1 if there are no processes
 */
int nextProcess(int *time){
	int x = 0;
	*time = 4; //quanta
	if (root == NULL)
		return -1;
	else
	{
		x = root -> value;
		removeProcess(x); 
		addProcess(x); //Add back
		return x;
	}
}

/**
 * Function that returns a boolean 1 True/0 False based on if there are any
 * processes still scheduled
 * @Return 1 if there are processes still scheduled 0 if there are no more
 *		scheduled processes
 */
int hasProcess(){

	if(root == NULL)
  	return 0;
 	else
 		return 1;
}
