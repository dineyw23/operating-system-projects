/*
Reference : http://www.tantalon.com/pete/cppopt/main.htm
*/

#include <stdio.h>
#include "cs1300bmp.h"
#include <iostream>
#include <stdint.h>
#include <fstream>
#include "Filter.h"

using namespace std;

#include "rtdsc.h"

//
// Forward declare the functions
//
Filter * readFilter(string filename);
double applyFilter(Filter *filter, cs1300bmp *input, cs1300bmp *output);

int main(int argc, char **argv)
{

    if ( argc < 2) {
        fprintf(stderr,"Usage: %s filter inputfile1 inputfile2 .... \n", argv[0]);
    }

    //
    // Convert to C++ strings to simplify manipulation
    //
    string filtername = argv[1];

    //
    // remove any ".filter" in the filtername
    //
    string filterOutputName = filtername;
    string::size_type loc = filterOutputName.find(".filter");
    if (loc != string::npos) {
        //
        // Remove the ".filter" name, which should occur on all the provided filters
        //
        filterOutputName = filtername.substr(0, loc);
    }

    Filter *filter = readFilter(filtername);

    double sum = 0.0;
    int samples = 0;

    for (int inNum = 2; inNum < argc; inNum++) {
        string inputFilename = argv[inNum];
        string outputFilename = "filtered-" + filterOutputName + "-" + inputFilename;
        struct cs1300bmp *input = new struct cs1300bmp;
        struct cs1300bmp *output = new struct cs1300bmp;
        int ok = cs1300bmp_readfile( (char *) inputFilename.c_str(), input);

        if ( ok ) {
            double sample = applyFilter(filter, input, output);
            sum += sample;
            samples++;
            cs1300bmp_writefile((char *) outputFilename.c_str(), output);
        }
    }
    fprintf(stdout, "Average cycles per sample is %f\n", sum / samples);

}

struct Filter *
readFilter(string filename)
{
    ifstream input(filename.c_str());

    if ( ! input.bad() )
    {
        int size = 0;
        input >> size;
        Filter *filter = new Filter(size);
        int div;
        input >> div;
        filter -> setDivisor(div);
        for (int i=0; i < size; i++) {
            for (int j=0; j < size; j++) {
                int value;
                input >> value;
                filter -> set(i,j,value);
            }
        }
        return filter;
    }

    return NULL;
}


#if defined(__arm__)
static inline unsigned int get_cyclecount (void)
{
 unsigned int value;
 // Read CCNT Register
 asm volatile ("MRC p15, 0, %0, c9, c13, 0\t\n": "=r"(value));
 return value;
}

static inline void init_perfcounters (int32_t do_reset, int32_t enable_divider)
{
 // in general enable all counters (including cycle counter)
 int32_t value = 1;

 // peform reset:
 if (do_reset)
 {
   value |= 2;     // reset all counters to zero.
   value |= 4;     // reset cycle counter to zero.
 }

 if (enable_divider)
   value |= 8;     // enable "by 64" divider for CCNT.

 value |= 16;

 // program the performance-counter control-register:
 asm volatile ("MCR p15, 0, %0, c9, c12, 0\t\n" :: "r"(value));

 // enable all counters:
 asm volatile ("MCR p15, 0, %0, c9, c12, 1\t\n" :: "r"(0x8000000f));

 // clear overflows:
 asm volatile ("MCR p15, 0, %0, c9, c12, 3\t\n" :: "r"(0x8000000f));
}



#endif

double applyFilter(struct Filter *filter, cs1300bmp *input, cs1300bmp *output)
{
  #if defined(__arm__)
  init_perfcounters (1, 1);
  #endif
  long long cycStart, cycStop;
  #if defined(__arm__)
  cycStart = get_cyclecount();
  #else
  cycStart = rdtscll();
  #endif

  //Smaller container
  const short int h = (input -> height) - 1;
  const short int w = (input -> width) - 1;

  output -> width = w + 1;
  output -> height = h + 1;

  int matrix[3][3];

  //Removed dependency on get. Now, do only 3 multiplications.
  //Saves multiplications. Not much difference but thought of this.
  //int zeroDim = 0 ;// filter -> dim; //Made everything public in filter.h
  int oneDim = filter -> dim;
  int twoDim = filter -> dim << 1;
  matrix[0][0] = filter -> data[0];
  matrix[0][1] = filter -> data[1];
  matrix[0][2] = filter -> data[2];
  matrix[1][0] = filter -> data[oneDim];
  matrix[1][1] = filter -> data[oneDim + 1];
  matrix[1][2] = filter -> data[oneDim + 2];
  matrix[2][0] = filter -> data[twoDim];
  matrix[2][1] = filter -> data[twoDim + 1];
  matrix[2][2] = filter -> data[twoDim + 2];

  //int rowUP = 0, rowDown = 0;
  //int temp1 = 0, temp2 = 0, temp3 = 0;
  //int colLeft = 0, colRight = 0;
/*If the object naturally changes every time through the loop, declare it within
the loop. If the object is constant throughout the loop,
 declare it outside the loop.*/


/*
  Two many if else but cannot convert to switch.
  Checking multiple conditions.
*/
  //hline
  if (matrix[2][1] == 2)
  {
    int plane = 0,row = 1;

    /* #pragma is a C/C++ language standard
        directive for special compiler definitions.
        This will utilize all the 4 cores of Raspberry Pi2.
        Declare int outside of loop for the private(row) thing.
    */

    #pragma omp parallel for private(row)
    for(plane = 0; plane < 3; ++plane)
    {
      for(row = 1; row < h; ++row)
      { //Reduced number of time by taking an extra varibale
        int rowUP = row - 1;int rowDown = row + 1;
        for(int col = 1; col < w; ++col)
        {//Unrolled
          int temp1 = 0, temp2 = 0, temp3 = 0;
          int colLeft = col - 1;int colRight = col + 1;
          temp1 += -(input->color[plane][rowUP][colLeft]);//-1
                                                      //0
          temp1 += input->color[plane][rowDown][colLeft];//1
          temp2 += -(input->color[plane][rowUP][col] << 1);//*-2
                                                        //0
          temp2 += input->color[plane][rowDown][col] << 1;//*2
          temp3 += -(input->color[plane][rowUP][colRight]);//-1
                                                      //0
          temp3 += input->color[plane][rowDown][colRight];//1
          temp1 = temp1 + temp2 + temp3;

          if (temp1 > 255)
            temp1 = 255;
          if (temp1 < 0)
            temp1 = 0;

          #pragma omp critical
          output -> color[plane][row][col] = temp1;
        }
      }
    }
  }
  // Gauss
	else if (matrix[1][1] == 8)
	{
		int plane, row;
		#pragma omp parallel for private(row)
		for(plane = 0; plane < 3; ++plane)
		{
			for(row = 1; row < h; ++row)
			{//Reduced number of time by taking an extra varibale
        int rowUP = row - 1; int rowDown = row + 1;
				for(int col = 1; col < w; ++col)
				{
					int temp1 = 0, temp2 = 0, temp3 = 0;
          int colLeft = col - 1; int colRight = col + 1;
					//0
					temp1 += input->color[plane][row][colLeft] << 2;//4
					//0
					temp2 += input->color[plane][rowUP][col] << 2;//4
					temp2 += input->color[plane][row][col] << 3;//8
					temp2 += input->color[plane][rowDown][col] << 2;//4
					temp3 += input->color[plane][row][colRight] << 2;//4

					//24
					temp1 = ((temp1 + temp2 + temp3) >> 3) / 3;
					if (temp1 > 255)
						temp1 = 255;
					if (temp1 < 0)
						temp1 = 0;
					#pragma omp critical
					output->color[plane][row][col] = temp1;
				}
			}
		}
	}
	// Emboss & avoid sharpen
	else if (matrix[1][2] == -1 && matrix[0][0] == 1)
	{
		int p;
		int plane,row;
		#pragma omp parallel for private(row)
		for(plane = 0; plane < 3; ++plane)
		{
			for(row = 1; row < h; ++row)
			{//Reduced number of time by taking an extra varibale
        int rowUP = row - 1;int rowDown = row + 1;
				for(int col = 1; col < w; ++col)
				{//Reduced number of time by taking an extra varibale
					int temp1 = 0, temp2 = 0, temp3 = 0;
          int colLeft = col - 1; int colRight = col + 1;
					temp1 += input->color[plane][rowUP][colLeft];
					temp1 += input->color[plane][row][colLeft];
					temp1 += input->color[plane][rowDown][colLeft];
					temp2 += input->color[plane][rowUP][col];
					temp2 += input->color[plane][row][col];
					temp2 += -(input->color[plane][rowDown][col]);
					temp3 += -(input->color[plane][rowUP][colRight]);//-1
					temp3 += -(input->color[plane][row][colRight]);//-1
					temp3 += -(input->color[plane][rowDown][colRight]);//-1

					temp1 = temp1 + temp2 + temp3;
					if (temp1 > 255)
						temp1 = 255;
					if (temp1 < 0)
						temp1 = 0;
					#pragma omp critical
					output->color[plane][row][col] = temp1;
				}
			}
		}
	}//Avg && avoid sharpen
  else if(matrix[1][1] == 1 && matrix[1][2] == 1)
	{
		int plane, row;
		#pragma omp parallel for private(row)
		for(plane = 0; plane < 3; ++plane)
		{
			for(row = 1; row < h; ++row)
			{//Reduced number of time by taking an extra varibale
        int rowUP = row - 1; int rowDown = row + 1;
				for(int col = 1; col < w; ++col)
				{//Unrolled , min variable
					int temp1 = 0, temp2 = 0, temp3 = 0;
          int colLeft = col - 1;int colRight = col + 1;
					//ALL 1 for avg
					temp1 += input->color[plane][rowUP][colLeft];
					temp1 += input->color[plane][row][colLeft];
					temp1 += input->color[plane][rowDown][colLeft];
					temp2 += input->color[plane][rowUP][col];
					temp2 += input->color[plane][row][col];
					temp2 += input->color[plane][rowDown][col];
					temp3 += input->color[plane][rowUP][colRight];
					temp3 += input->color[plane][row][colRight];
					temp3 += input->color[plane][rowDown][colRight];

					/*temp1 = ((int)((temp1 + temp2 + temp3) >> 3))
              - (((temp1 + temp2 + temp3) & 7));*/
           temp1 = (temp1 + temp2 + temp3) / 9;
            // n/9 ======== >>>> n /8 - n / 72
					if (temp1 < 0)
						temp1 = 0;
					if (temp1 > 255)
						temp1 = 255;
					#pragma omp critical
					output->color[plane][row][col] = temp1;
				}
			}
		}
	}
  else if(matrix[0][0] == 11) //Sharpen
  {
    int plane, row;
		#pragma omp parallel for private(row)
		for(plane = 0; plane < 3; ++plane)
		{
			for(row = 1; row < h; ++row)
			{//Reduced number of time by taking an extra varibale
        int rowUP = row - 1;int rowDown = row + 1;
				for(int col = 1; col < w; ++col)
				{//Unrolled , min variable
					int temp1 = 0, temp2 = 0, temp3 = 0;
          int colLeft = col - 1;int colRight = col + 1;
					temp1 += (input->color[plane][rowUP][colLeft] << 4)//16
          - (input->color[plane][rowUP][colLeft] << 2) //-4
          - (input->color[plane][rowUP][colLeft]);//-1
					temp1 += -(input->color[plane][row][colLeft]);
					temp1 += -(input->color[plane][rowDown][colLeft]);
					temp2 += ((input->color[plane][rowUP][col] << 3)
          + (input->color[plane][rowUP][col] << 1));
					temp2 += -(input->color[plane][row][col]);
					temp2 += -(input->color[plane][rowDown][col]);
					temp3 += input->color[plane][rowUP][colRight];
					temp3 += -(input->color[plane][row][colRight]);
					temp3 += -(input->color[plane][rowDown][colRight]);

					temp1 = ((temp1 + temp2 + temp3) >> 2)/ 5;
					if (temp1 < 0)
						temp1 = 0;
					if (temp1 > 255)
						temp1 = 255;
					#pragma omp critical
					output->color[plane][row][col] = temp1;
				}
			}
		}
  }
  else if(matrix[1][0] == -2)//VLine
  {
    int plane, row;
    #pragma omp parallel for private(row)
    for(plane = 0; plane < 3; ++plane)
    {
      for(row = 1; row < h; ++row)
      {//Reduced number of time by taking an extra varibale
        int rowUP = row - 1;int rowDown = row + 1;
        for(int col = 1; col < w; ++col)
        {//Unrolled , min variable
          int  temp1 = 0, temp2 = 0, temp3 = 0;
          int colLeft = col - 1;int colRight = col + 1;

          temp1 += -(input->color[plane][rowUP][colLeft]);
          temp1 += -(input->color[plane][row][colLeft] << 1);
          temp1 += -(input->color[plane][rowDown][colLeft]);
          temp3 += input->color[plane][rowUP][colRight];
          temp3 += input->color[plane][row][colRight] << 1;
          temp3 += input->color[plane][rowDown][colRight];

          temp1 += (temp2 + temp3);
          if (temp1 < 0)
            temp1 = 0;
          if (temp1 > 255)
            temp1 = 255;
          #pragma omp critical
          output->color[plane][row][col] = temp1;
        }
      }
    }
  }
  else if(matrix[1][1] == -7)//Edge
  {
    int plane, row;
		#pragma omp parallel for private(row)
		for(plane = 0; plane < 3; ++plane)
		{
			for(row = 1; row < h; ++row)
			{//Reduced number of time by taking an extra varibale
        int rowUP = row - 1;int rowDown = row + 1;
				for(int col = 1; col < w; ++col)
				{//Unrolled , min variable
					int temp1 = 0, temp2 = 0, temp3 = 0;
          int colLeft = col - 1;int colRight = col + 1;
					temp1 += input->color[plane][rowUP][colLeft];
					temp1 += input->color[plane][row][colLeft];
					temp1 += input->color[plane][rowDown][colLeft];
					temp2 += input->color[plane][rowUP][col];
					temp2 += (-(input->color[plane][row][col]) << 3)
          + (input->color[plane][row][col]);
					temp2 += input->color[plane][rowDown][col];
					temp3 += input->color[plane][rowUP][colRight];
					temp3 += input->color[plane][row][colRight];
					temp3 += input->color[plane][rowDown][colRight];

					temp1 += (temp2 + temp3);
					if (temp1 < 0)
						temp1 = 0;
					if (temp1 > 255)
						temp1 = 255;
					#pragma omp critical
					output->color[plane][row][col] = temp1;
				}
			}
		}
  }

  #if defined(__arm__)
  cycStop = get_cyclecount();
  #else
  cycStop = rdtscll();
  #endif

  double diff = cycStop-cycStart;
  diff = diff * 64;
  double diffPerPixel = diff / (output -> width * output -> height);
  fprintf(stderr, "Took %f cycles to process, or %f cycles per pixel\n",
    diff, diff / (output -> width * output -> height));
  return diffPerPixel;
}
