## Improving Code Performance of image filtering program (Wrote FilterMain.cpp)

    - Used loop-unrolling
    - Additional build flags
    - Less function calls
    - Taking advantage of multiple processors