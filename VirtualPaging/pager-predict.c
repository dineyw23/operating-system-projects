/*
 * File: pager-predict.c
 * Author:       Andy Sayler
 *               http://www.andysayler.com
 * Adopted From: Dr. Alva Couch
 *               http://www.cs.tufts.edu/~couch/
 *
 * Project: CSCI 3753 Programming Assignment 4
 * Create Date: Unknown
 * Modify Date: 2012/04/03
 * Description:
 * 	This file contains a predictive pageit
 *      implmentation.
 */

#include <stdio.h>
#include <stdlib.h>

#include "simulator.h"

/*Page state to maintain the frequency ? May be array? */
struct page_stat
{
  int page;
  int freq;
};
typedef struct page_stat Info;

/*node which stores everything*/
/*A process, if it has been swapped in or not, previous page
and Info about pages we will predict. All the current pages
of last(previous) page*/
/*Some sort of a storage*/
struct node
{
  Info pages[MAXPROCPAGES][MAXPROCPAGES];
  int swap_status;
  int prev;
  int next;
 };

typedef struct node storage;

/* Insert into the cfg, given the last_page, the cur_page and
 * the proc. For example, if cur_page=1, last_page=0, and proc=9,
 * then '1' will be inserted into the cfg for page zero, proc 9,
 * signalling that sometime in the past, proc 9 went from page 0
 * to page 1.
 */
void record(storage all[MAXPROCESSES],int proc,int cpage,int ppage)
{
  int i;
  for(i = 0; i < MAXPROCESSES; i++)
  {
    if(all[proc].pages[ppage][i].page == -1)
    {
      all[proc].pages[ppage][i].freq = 1;
      all[proc].pages[ppage][i].page = cpage;
      break;
    }
    if(all[proc].pages[ppage][i].page == cpage)
    {
      all[proc].pages[ppage][i].freq++;
      break;
    }
  }
}
/*Sort by frequency*/
Info* sort(Info* guesses,int count)
{/*tRYING TO SORT by freq but not much differnce*/
  Info temp;
  int i;
  for(i = 0; i < count; i++)
  {
    if(guesses[i].page != -1
       && guesses[i+1].page != -1
       && guesses[i].freq  < guesses[i+1].freq)
    {
      temp = guesses[i];
      guesses[i] = guesses[i+1];
      guesses[i+1] = temp;
    }
  }
  return guesses;
}
int* bsort(int timestamps[MAXPROCESSES][MAXPROCPAGES],int proc)
{/*TRYING TO SORT but not much differnce*/
  int local[MAXPROCPAGES] = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
  int x = 0;
  int temp = 2147483647;
  int i;
  for(i = 0; i < MAXPROCPAGES; i++)
  {
    if(timestamps[proc][i] < temp)
    {
      temp  = timestamps[proc][i];
      x = local[i];
      local[i] = local[i+1];
      local[i+1] = x;
    }
  }
  int* r = local;
  return r;
}
void pageit(Pentry q[MAXPROCESSES])
{

    /* This file contains the stub for an LRU pager */
    /* You may need to add/remove/modify any part of this file */

    /* Static vars */
    static int initialized = 0;
    static int tick = 1; // artificial time
    static int timestamps[MAXPROCESSES][MAXPROCPAGES];
    static storage all[MAXPROCESSES];

    /* Local vars */
    int proctmp;
    int pagetmp;
    int ppage;
    int npage;
    int cpage;
    int pctmp,to_evict;
    int check = 0;
    /* initialize static vars on first run */
    if(!initialized)
    {
      int x;
        for(proctmp=0; proctmp < MAXPROCESSES; proctmp++)
        {
            for(pagetmp=0; pagetmp < MAXPROCPAGES; pagetmp++)
            {
                for(x = 0; x < MAXPROCPAGES; x++)
                {
                  timestamps[proctmp][pagetmp] = 0;
                  all[proctmp].pages[pagetmp][x].page = -1;
                }
            }
            all[proctmp].swap_status = 0;
            all[proctmp].prev = -1;
            all[proctmp].next = -1;
        }
        initialized = 1;
    }
    /*Calculate Previous page and if not equal current pageout
    This mat not be required further*/
    for(proctmp = 0; proctmp < MAXPROCESSES; proctmp++)
    {
      if(q[proctmp].active)
      {
        ppage = all[proctmp].prev;        //Previous page
        cpage = (q[proctmp].pc)/PAGESIZE; //Current Page
        npage = all[proctmp].next;

        all[proctmp].next = (q[proctmp].pc+1)/PAGESIZE;
        all[proctmp].prev = q[proctmp].pc/PAGESIZE;
        timestamps[proctmp][cpage] = tick + 1;

        if(ppage == -1)
          continue;
        if(cpage == npage)
        {
          /*May be it is in or not -> Just try to swap in*/
          if(!q[proctmp].pages[npage])
            pagein(proctmp,npage);
        }
        if(ppage != cpage)
        {
          pageout(proctmp, ppage);
        }
        record(all,proctmp,cpage, ppage);
      }

      /*Future Predictions*/
      /*Get all the pages(array) from the current(local) process
      for the next 100 ticks. All the current pages
      of the previous page */

      if(q[proctmp].active)
      {
        Info *guesses;
        guesses = all[proctmp].pages[(q[proctmp].pc+100)/PAGESIZE];
        int count = 0;
        for(count = 0; count < MAXPROCPAGES; count++)
          if(guesses[count].page != -1 )
            count++;
        /*Sort by frequency*/
        guesses = sort(guesses,count);
        /* Pagein all the pages that we have guessed. */
        int i;
        int* local1;
        for(i=0; i < count; i++)
        {
          if(q[proctmp].active)
          if(!q[proctmp].pages[guesses[i].page])
          {
            if(!pagein(proctmp, guesses[i].page))
            { /* If pagein fails, select a page to evict, LRU*/
              int temp = 2147483647;
    				  int least = 0;
              int c = 0;
              if(!all[proctmp].swap_status)
              {
                for(pagetmp = 0; pagetmp < q[proctmp].npages; pagetmp++)
                {
                  if(!q[proctmp].pages[pagetmp])
                  {
                    c++;//If 20, none of the pages swapped in.
                    continue;
                  }
                  if(timestamps[proctmp][pagetmp] < temp)
                  {

                    temp = timestamps[proctmp][pagetmp];
                    least = pagetmp;
                  }
                  to_evict = least;
                }
                local1 = bsort(timestamps,proctmp);
                if(c < 20) //If none of the pages of proctmp are swapped in
                {          //continue
                  if(cpage != to_evict && to_evict != guesses[i].page
                      && to_evict != ppage)
                      if(!pageout(proctmp, to_evict))
                      {
                        to_evict = local1[18];
                        if(cpage != to_evict && to_evict != guesses[i].page)
                          pageout(proctmp,to_evict);
                      }
                }
                else
                  continue;
                all[proctmp].swap_status = 1;
              }
            }
          }
        }
      }
    /*SAME AS LRU*/
    /*Find a LRU page to swap out for the page the process needs*/
    int old;
    if(check != 1 && q[proctmp].active)
    {
      pctmp = (q[proctmp].pc);
      pagetmp = pctmp/PAGESIZE;
      old = pctmp/PAGESIZE;
      timestamps[proctmp][pagetmp] = tick + 1;
      if(!q[proctmp].pages[pagetmp])
      {
        if(!pagein(proctmp,pagetmp))
        {
          if(!all[proctmp].swap_status)
          {
            int* local;
            int temp = 2147483647;
            int least = 0;
            int c = 0;
      			for(pagetmp = 0; pagetmp < q[proctmp].npages; pagetmp++)
      			{
    					if(!q[proctmp].pages[pagetmp])
              {
                c++;
                continue;
              }
    					if(timestamps[proctmp][pagetmp] < temp)
              {
                temp = timestamps[proctmp][pagetmp];
    						least = pagetmp;
      				}
    				}
            to_evict = least;
            local = bsort(timestamps,proctmp);
            if(c < 20)
            {
              if(to_evict != old && to_evict != ppage && to_evict != npage)
              {
                if(!pageout(proctmp, to_evict))
                {
                  to_evict = local[18];
                  if(to_evict != old)
                  {
                    pageout(proctmp, to_evict);
                  }
                }
              }
            }
            all[proctmp].swap_status = 1;
          }
        }
        else
          all[proctmp].swap_status = 0;
        }
      }
      else
      {
        /*Pageout all the inactive ones*/
        for(pagetmp = 0; pagetmp < q[proctmp].npages; pagetmp++)
          pageout(proctmp,pagetmp);
      }
    }
    /* advance time for next pageit iteration */
    tick++;
}
