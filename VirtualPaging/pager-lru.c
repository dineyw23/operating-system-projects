/*
 * File: pager-lru.c
 * Author:       Andy Sayler
 *               http://www.andysayler.com
 * Adopted From: Dr. Alva Couch
 *               http://www.cs.tufts.edu/~couch/
 *
 * Project: CSCI 3753 Programming Assignment 4
 * Create Date: Unknown
 * Modify Date: 2012/04/03
 * Description:
 * 	This file contains an lru pageit
 *      implmentation.
 */

#include <stdio.h>
#include <stdlib.h>

#include "simulator.h"

void pageit(Pentry q[MAXPROCESSES]) {

	/* This file contains the stub for an LRU pager */
	/* You may need to add/remove/modify any part of this file */

	/* Static vars */
	static int initialized = 0;
	static int tick = 1; // artificial time
	static int timestamps[MAXPROCESSES][MAXPROCPAGES];
	/* Local vars */
	int proctmp;
	int pagetmp;
	int pctmp;
	int to_evict = 0;

	/* initialize static vars on first run */
	if (!initialized)
	{
		for (proctmp = 0; proctmp < MAXPROCESSES; proctmp++)
		{
			for (pagetmp = 0; pagetmp < MAXPROCPAGES; pagetmp++)
			{
				timestamps[proctmp][pagetmp] = 0;
			}
		}
		initialized = 1;
	}

	for (proctmp = 0; proctmp < MAXPROCESSES; proctmp++)
	{
		if(q[proctmp].active)
		{
			pctmp = q[proctmp].pc;	//PC temp
			pagetmp = pctmp/PAGESIZE; //Requested page
			timestamps[proctmp][pagetmp] = tick;

			int reqd = pagetmp;

			/* Is page swaped-in? */
			if (!q[proctmp].pages[pagetmp])
			{	/* Try to swap in */
				if(!pagein(proctmp,pagetmp))
				{ /*Failure paging in -> Search for LRU page to be evicted*/
					int least = 0;
				  int temp =  2147483647; //MAX Value for INT Cannot be greater
					for(pagetmp = 0; pagetmp < q[proctmp].npages; pagetmp++)
					{	//Only if already swapped in
						if(!q[proctmp].pages[pagetmp])
							continue;
						if(timestamps[proctmp][pagetmp] < temp)
						{
							temp = timestamps[proctmp][pagetmp];
							least = pagetmp;
						}
					}
					to_evict = least;
					if(to_evict != reqd)
					{
						pageout(proctmp, to_evict);
					}
				}
			}
		}
	}
	/* advance time for next pageit iteration */
	tick++;
}
