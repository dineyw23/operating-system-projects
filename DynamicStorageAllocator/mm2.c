#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include "mm.h"
#include "memlib.h"

team_t team = {
  /* Team name */
  "ChicoState",
  /* First member's full name */
  "Diney Wankhede",
  /* First member's email address */
  "dwankhede@mail.csuchico.edu",
  /* (leave blank) */
  "",
  /* (leave blank) */
  ""
};
////////////////////////////////////////////////////////////////////////////
// Constants and macros
//
// These correspond to the material in Figure 9.43 of the text
// The macros have been turned into C++ inline functions to
// make debugging code easier.
//
/////////////////////////////////////////////////////////////////////////////

#define WSIZE     4          // word and header/footer size (bytes)
#define DSIZE     8          // double word size (bytes)
#define INITCHUNKSIZE (1<<6)
#define CHUNKSIZE (1<<12)

#define ALIGNMENT 8
#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~0x7)
#define LISTS 5
#define REALLOC_BUFFER  (1<<7)

static inline int MAX(int x, int y) {
  return x > y ? x : y;
}

static inline int MIN(int x, int y) {
  return x < y ? x : y;
}
// Pack a size and allocated bit into a word
//Unmasked the alloc bit. That's the main part!!
static inline size_t PACK(size_t size, int alloc) {
  return ((size) | (alloc));
}
// Read and write a word at address p
#define GET(p)            (*(unsigned int *)(p))
#define PUT(p, val)       (*(unsigned int *)(p) = (val) | GET_TAG(p))
static inline void PUT_NOTAG( void *p, size_t val)
{
  *((size_t *)p) = val;
}

// Store predecessor or successor pointer for free blocks
#define SET_PTR(p, ptr) (*(unsigned int *)(p) = (unsigned int)(ptr))

static inline size_t GET_SIZE( void *p )
{
  return GET(p) & ~0x7;
}

static inline int GET_ALLOC( void *p  )
{
  return GET(p) & 0x1;
}
static inline int GET_TAG( void *p  )
{
  return GET(p) & 0x2;
}
static inline int SET_RATAG( void *p )
{
  return GET(p) |= 0x2;
}

static inline int REMOVE_RATAG( void *p)
{
  return GET(p) &= ~0x2;
}

static inline void *HDRP(void *bp)
{
  return ( (char *)bp) - WSIZE;
}

static inline void *FTRP(void *bp)
{
  return ((char *)(bp) + GET_SIZE(HDRP(bp)) - DSIZE);
}
static inline void *NEXT_BLKP(void *bp)
{
  return  ((char *)(bp) + GET_SIZE(((char *)(bp) - WSIZE)));
}

static inline void* PREV_BLKP(void *bp)
{
  return  ((char *)(bp) - GET_SIZE(((char *)(bp) - DSIZE)));
}

static inline void* PRED_PTR(void* bp)
{
    return (char*)bp;
}
static inline void* SUCC_PTR(void* bp)
{
    return (char*)(bp+WSIZE);
}
static inline void* PRED(void* bp)
{
    return (*(char**)bp);
}
static inline void* SUCC(void* bp)
{
    return (*(char**)(SUCC_PTR(bp)));
}

// Global var
void *segregated_list[LISTS];


// Functions
static void *extend_heap(size_t size);
static void *coalesce(void *ptr);
static void *place(void *ptr, size_t asize);
static void addNode(void *ptr, size_t size);
static void removeNode(void *ptr);

//static void checkheap(int verbose);

static void *extend_heap(size_t words)
{
    void *bp;
    size_t asize;                // Adjusted size

    asize = ALIGN(words);
    if ((bp = mem_sbrk(asize)) == (void *)-1)
        return NULL;

/* Initialize free block header/footer and the epilogue header */
    PUT_NOTAG(HDRP(bp), PACK(asize, 0));
    PUT_NOTAG(FTRP(bp), PACK(asize, 0));
    PUT_NOTAG(HDRP(NEXT_BLKP(bp)), PACK(0, 1));
    addNode(bp, asize);
     /* Coalesce if the previous block was free */
    return coalesce(bp);
}

static void addNode(void *bp, size_t size)
{
    int list = 0;
    void *searchNode = bp;
    void *addNodePtr = NULL;
    int i =0;

    for(i = 0; i < (LISTS - 1); i++)
    {
        if(size > 1)
        {
          size >>= 1;
          list++;
        }
    }

    // Keep size ascending order and search
    searchNode = segregated_list[list];
    while((searchNode != NULL)&& (size > GET_SIZE(HDRP(searchNode))))
    {
      addNodePtr = searchNode;
      searchNode = PRED(searchNode);
    }

    if (searchNode == NULL && addNodePtr == NULL)
    {
      SET_PTR(PRED_PTR(bp), NULL);
      SET_PTR(SUCC_PTR(bp), NULL);
      segregated_list[list] = bp;
    }
    else if(searchNode == NULL && addNodePtr != NULL)
    {
      SET_PTR(PRED_PTR(bp), NULL);
      SET_PTR(SUCC_PTR(bp), addNodePtr);
      SET_PTR(PRED_PTR(addNodePtr), bp);

    }
    else if(searchNode != NULL && addNodePtr == NULL)
    {
      SET_PTR(PRED_PTR(bp), searchNode);
      SET_PTR(SUCC_PTR(searchNode), bp);
      SET_PTR(SUCC_PTR(bp), NULL);
      segregated_list[list] = bp;
    }
    else if(searchNode != NULL && addNodePtr != NULL)
    {
      SET_PTR(PRED_PTR(bp), searchNode);
      SET_PTR(SUCC_PTR(searchNode), bp);
      SET_PTR(SUCC_PTR(bp), addNodePtr);
      SET_PTR(PRED_PTR(addNodePtr), bp);
    }
    return;
}


static void removeNode(void *bp)
{
    int list = 0;
    size_t size = GET_SIZE(HDRP(bp));
    int i;

    for(i = 0; i < (LISTS - 1); i++)
    {
        if(size > 1)
        {
          size >>= 1;
          list++;
        }
    }
    if (PRED(bp) == NULL && SUCC(bp) == NULL)
    {
      segregated_list[list] = NULL;
    }
    else if(PRED(bp) == NULL && SUCC(bp) != NULL)
    {
        SET_PTR(PRED_PTR(SUCC(bp)), NULL);
    }
    else if(PRED(bp) != NULL && SUCC(bp) == NULL)
    {
      SET_PTR(SUCC_PTR(PRED(bp)), NULL);
      segregated_list[list] = PRED(bp);
    }
    else if(PRED(bp) != NULL && SUCC(bp) != NULL)
    {
      SET_PTR(SUCC_PTR(PRED(bp)), SUCC(bp));
      SET_PTR(PRED_PTR(SUCC(bp)), PRED(bp));
    }

    return;
}


static void *coalesce(void *bp)
{
    size_t prev_alloc = GET_ALLOC(HDRP(PREV_BLKP(bp)));
    size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(bp)));
    size_t size = GET_SIZE(HDRP(bp));


    // Do not coalesce with previous block if the previous block is tagged with Reallocation tag
    if (GET_TAG(HDRP(PREV_BLKP(bp))))
        prev_alloc = 1;

    if (prev_alloc && next_alloc)
    {                         // Case 1
        return bp;
    }
    else if (prev_alloc && !next_alloc)
    {                   // Case 2
        removeNode(bp);
        removeNode(NEXT_BLKP(bp));
        size += GET_SIZE(HDRP(NEXT_BLKP(bp)));
        PUT(HDRP(bp), PACK(size, 0));
        PUT(FTRP(bp), PACK(size, 0));
    }
    else if (!prev_alloc && next_alloc)
    {                 // Case 3
        removeNode(bp);
        removeNode(PREV_BLKP(bp));
        size += GET_SIZE(HDRP(PREV_BLKP(bp)));
        PUT(FTRP(bp), PACK(size, 0));
        PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
        bp = PREV_BLKP(bp);
    }
     else
     {                                                // Case 4
        removeNode(bp);
        removeNode(PREV_BLKP(bp));
        removeNode(NEXT_BLKP(bp));
        size += GET_SIZE(HDRP(PREV_BLKP(bp))) + GET_SIZE(HDRP(NEXT_BLKP(bp)));
        PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
        PUT(FTRP(NEXT_BLKP(bp)), PACK(size, 0));
        bp = PREV_BLKP(bp);
    }

    addNode(bp, size);

    return bp;
}

static void *place(void *ptr, size_t asize)
{
    size_t ptr_size = GET_SIZE(HDRP(ptr));
    size_t remainder = ptr_size - asize;

    removeNode(ptr);


    if (remainder > DSIZE * 2)
    {
      if(ptr_size < 100)
      {
        // Split block
        PUT(HDRP(ptr), PACK(asize, 1));
        PUT(FTRP(ptr), PACK(asize, 1));
        PUT_NOTAG(HDRP(NEXT_BLKP(ptr)), PACK(remainder, 0));
        PUT_NOTAG(FTRP(NEXT_BLKP(ptr)), PACK(remainder, 0));
        addNode(NEXT_BLKP(ptr), remainder);
      }
      else
      {
        // Split block
        PUT(HDRP(ptr), PACK(remainder, 0));
        PUT(FTRP(ptr), PACK(remainder, 0));
        PUT_NOTAG(HDRP(NEXT_BLKP(ptr)), PACK(asize, 1));
        PUT_NOTAG(FTRP(NEXT_BLKP(ptr)), PACK(asize, 1));
        addNode(ptr, remainder);
        return NEXT_BLKP(ptr);
      }}
      else
      {
        // Do not split block
        PUT(HDRP(ptr), PACK(ptr_size, 1));
        PUT(FTRP(ptr), PACK(ptr_size, 1));
      }

    return ptr;
}



//////////////////////////////////////// End of Helper functions ////////////////////////////////////////






/*
 * mm_init - initialize the malloc package.
 * Before calling mm_malloc, mm_realloc, or mm_free,
 * the application program calls mm_init to perform any necessary initializations,
 * such as allocating the initial heap area.
 *
 * Return value : -1 if there was a problem, 0 otherwise.
 */
int mm_init(void)
{
    int list;
    char *heap_start; // Pointer to beginning of heap

    // Initialize segregated free lists
    for (list = 0; list < LISTS; list++) {
        segregated_list[list] = NULL;
    }

    // Allocate memory for the initial empty heap
    if ((long)(heap_start = mem_sbrk(4 * WSIZE)) == -1)
        return -1;

    PUT_NOTAG(heap_start, 0);                            /* Alignment padding */
    PUT_NOTAG(heap_start + (1 * WSIZE), PACK(DSIZE, 1)); /* Prologue header */
    PUT_NOTAG(heap_start + (2 * WSIZE), PACK(DSIZE, 1)); /* Prologue footer */
    PUT_NOTAG(heap_start + (3 * WSIZE), PACK(0, 1));     /* Epilogue header */

    if (extend_heap(INITCHUNKSIZE) == NULL)
        return -1;

    return 0;
}

/*
 * mm_malloc - Allocate a block by incrementing the brk pointer.
 *     Always allocate a block whose size is a multiple of the alignment.
 *
 * Role :
 * 1. The mm_malloc routine returns a pointer to an allocated block payload.
 * 2. The entire allocated block should lie within the heap region.
 * 3. The entire allocated block should overlap with any other chunk.
 *
 * Return value : Always return the payload pointers that are alligned to 8 bytes.
 */
void *mm_malloc(size_t size)
{
    size_t asize;      /* Adjusted block size */
    size_t extendsize; /* Amount to extend heap if no fit */
    void *ptr = NULL;  /* Pointer */

    // Ignore size 0 cases
    if (size == 0)
        return NULL;

    // Align block size
    if (size <= DSIZE) {
        asize = 2 * DSIZE;
    } else {
        asize = ALIGN(size+DSIZE);
    }

    int list = 0;
    size_t searchsize = asize;
    // Search for free block in segregated list
    while (list < LISTS) {
        if ((list == LISTS - 1) || ((searchsize <= 1) && (segregated_list[list] != NULL))) {
            ptr = segregated_list[list];
            // Ignore blocks that are too small or marked with the reallocation bit
            while ((ptr != NULL) && ((asize > GET_SIZE(HDRP(ptr))) || (GET_TAG(HDRP(ptr)))))
            {
                ptr = PRED(ptr);
            }
            if (ptr != NULL)
                break;
        }

        searchsize >>= 1;
        list++;
    }

    // if free block is not found, extend the heap
    if (ptr == NULL) {
        extendsize = MAX(asize, CHUNKSIZE);

        if ((ptr = extend_heap(extendsize)) == NULL)
            return NULL;
    }

    // Place and divide block
    ptr = place(ptr, asize);


    // Return pointer to newly allocated block
    return ptr;
}

/*
 * mm_free - Freeing a block does nothing.
 *
 * Role : The mm_free routine frees the block pointed to by ptr
 *
 * Return value : returns nothing
 */
void mm_free(void *ptr)
{
    size_t size = GET_SIZE(HDRP(ptr));

    REMOVE_RATAG(HDRP(NEXT_BLKP(ptr)));
    PUT(HDRP(ptr), PACK(size, 0));
    PUT(FTRP(ptr), PACK(size, 0));

    addNode(ptr, size);
    coalesce(ptr);

    return;
}

/*
 * mm_realloc - Implemented simply in terms of mm_malloc and mm_free
 *
 * Role : The mm_realloc routine returns a pointer to an allocated
 *        region of at least size bytes with constraints.
 *
 *  I used https://github.com/htian/malloc-lab/blob/master/mm.c source idea to maximize utilization
 *  by using reallocation tags
 *  in reallocation cases (realloc-bal.rep, realloc2-bal.rep)
 */
void *mm_realloc(void *ptr, size_t size)
{
    void *new_ptr = ptr;    /* Pointer to be returned */
    size_t new_size = size; /* Size of new block */
    int remainder;          /* Adequacy of block sizes */
    int extendsize;         /* Size of heap extension */
    int block_buffer;       /* Size of block buffer */

    // Ignore size 0 cases
    if (size == 0)
        return NULL;

    // Align block size
    if (new_size <= DSIZE) {
        new_size = 2 * DSIZE;
    } else {
        new_size = ALIGN(size+DSIZE);
    }

    /* Add overhead requirements to block size *;
    new_size += REALLOC_BUFFER;

    /* Calculate block buffer */
    block_buffer = GET_SIZE(HDRP(ptr)) - new_size;

    /* Allocate more space if overhead falls below the minimum */
    if (block_buffer < 0) {
        /* Check if next block is a free block or the epilogue block */
        if (!GET_ALLOC(HDRP(NEXT_BLKP(ptr))) || !GET_SIZE(HDRP(NEXT_BLKP(ptr))))
        {
            remainder = GET_SIZE(HDRP(ptr)) + GET_SIZE(HDRP(NEXT_BLKP(ptr))) - new_size;
            if (remainder < 0) {
                extendsize = MAX(-remainder, CHUNKSIZE);
                if (extend_heap(extendsize) == NULL)
                    return NULL;
                remainder += extendsize;
            }

            removeNode(NEXT_BLKP(ptr));

            // Do not split block
            PUT_NOTAG(HDRP(ptr), PACK(new_size + remainder, 1));
            PUT_NOTAG(FTRP(ptr), PACK(new_size + remainder, 1));
        } else {
            new_ptr = mm_malloc(new_size - DSIZE);
            memcpy(new_ptr, ptr, MIN(size, new_size));
            mm_free(ptr);
        }
        block_buffer = GET_SIZE(HDRP(new_ptr)) - new_size;
    }

    // Tag the next block if block overhead drops below twice the overhead
    if (block_buffer < 2 * REALLOC_BUFFER)
        SET_RATAG(HDRP(NEXT_BLKP(new_ptr)));

    // Return the reallocated block
    return new_ptr;
}



















*
* mm.c : Segregated List based on explicit free lists.
*      20 Linked List. Idea from book and  Stack overflow.
      http://stackoverflow.com/questions/25735905/
      implementing-segregated-memory-storage-malloc-in-c
*      Each block has header and footer.
*         31                     3  2  1  0
*         -----------------------------------
*       | s  s  s  s  ... s  s  s  0  r  a/f
*        -----------------------------------
* where s are the meaningful size bits and a/f is set
* iff the block is allocated. r is set iff the block is reallocated.
* Footer contains size and allocation information.
*  Segregated lists are organized in power of 2.
*
*   <<FREE BLOCK>>
*
*   31 30                               3   2   1   0
*   .................................................
*   .  s s  s  s  s                            r  a/f  <- header
*   ................................................. <-- bp
*   .         predecessor                           .
*   .................................................<-- bp _WSIZE
*   .        successor                              .
*   .................................................
*   .                                               . Footer
*   .................................................
*
*
*/
#include <stdio.h>
#include <stdlib.h>
#include <assert.h>
#include <unistd.h>
#include <string.h>
#include "mm.h"
#include "memlib.h"


team_t team = {
  /* Team name */
  "ChicoState",
  /* First member's full name */
  "Diney Wankhede",
  /* First member's email address */
  "dwankhede@mail.csuchico.edu",
  /* (leave blank) */
  "",
  /* (leave blank) */
  ""
};

#define WSIZE     4          // word and header/footer size (bytes)
#define DSIZE     8          // double word size (bytes)
#define CHUNKSIZE (1<<12)
#define START (1<<6) //Extend initially
#define ALIGNMENT 8   //requirements
#define LISTS 20
#define REALLOC  (1<<8)

/* Static Inline functions */
static inline size_t ALIGN(size_t size){
    return (((size) + (ALIGNMENT-1)) & ~0x7);
}
static inline int MAX(int x, int y){
  return x > y ? x : y;
}
static inline int MIN(int x, int y){
  return x < y ? x : y;
}
// Pack a size and allocated bit into a word
//Unmasked the alloc bit. That's the main part!!
static inline size_t PACK(size_t size, int alloc){
  return ((size) | (alloc));
}// Read and write a word at address p
#define GET(p)            (*(unsigned int *)(p))
#define PUT(p, val)       (*(unsigned int *)(p) = (val) | GET_TAG(p))
static inline void PUT_NO_TAG(void *p, size_t val){
  *((size_t *)p) = val;
}

#define SETP(p, ptr) (*(unsigned int *)(p) = (unsigned int)(ptr))
static inline size_t GET_SIZE( void *p ){
  return GET(p) & ~0x7;
}
static inline int GET_ALLOC(void *p){
  return GET(p) & 0x1;
}
static inline int GET_TAG(void *p){
  return GET(p) & 0x2;
}
static inline int SET_RATAG( void *p ){
  return GET(p) |= 0x2;
}
static inline int REMOVE_RATAG( void *p){
  return GET(p) &= ~0x2;
}
static inline void *HDRP(void *bp){
  return ( (char *)bp) - WSIZE;
}
static inline void *FTRP(void *bp){
  return ((char *)(bp) + GET_SIZE(HDRP(bp)) - DSIZE);
}
static inline void *NEXT_BLKP(void *bp){
  return  ((char *)(bp) + GET_SIZE(((char *)(bp) - WSIZE)));
}

static inline void* PREV_BLKP(void *bp){
  return  ((char *)(bp) - GET_SIZE(((char *)(bp) - DSIZE)));
}
static inline void* PRED_PTR(void* bp){
    return (char*)bp;
}
static inline void* SUCC_PTR(void* bp){
    return (char*)(bp+WSIZE);
}
static inline void* PRED(void* bp){
    return (*(char**)bp);
}
static inline void* SUCC(void* bp){
    return (*(char**)(SUCC_PTR(bp)));
}

// Global var : Creating a list of 5.
void *segregated_list[LISTS];

// Functions
static void *extend_heap(size_t size);
static void *coalesce(void *ptr);
//Changed the return type to maintain the pointer in seg list.
static void *place(void *ptr, size_t asize);
//Simple   linked list add, uses inline functions
static void addNode(void *ptr, size_t size);
//Simple   linked list removeNode, uses inline functions
static void removeNode(void *ptr);

//Additional Functions
//Simple add to node seg list
static void addNode(void *bp, size_t size)
{
    void *searchNode = bp;
    void *addNodePtr = NULL;
    int list = 0;
    int i;
    for(i = 0; i < (LISTS - 1); i++)
    {//Select seg list
      if(size > 1)
      {
        size = size >> 1;
        list++;
      }
    }
    searchNode = segregated_list[list];

    if (searchNode == NULL && addNodePtr == NULL)
    {// First node in that particular list & set PRED & SUCC
      SETP(PRED_PTR(bp), NULL);
      SETP(SUCC_PTR(bp), NULL);
      segregated_list[list] = bp;
    }
    else if(searchNode == NULL && addNodePtr != NULL)
    { //At the begining
      SETP(PRED_PTR(bp), NULL);
      SETP(SUCC_PTR(bp), addNodePtr);
      SETP(PRED_PTR(addNodePtr), bp);
    }
    else if(searchNode != NULL && addNodePtr == NULL)
    {//At the end
      SETP(PRED_PTR(bp), searchNode);
      SETP(SUCC_PTR(searchNode), bp);
      SETP(SUCC_PTR(bp), NULL);
      segregated_list[list] = bp;
    }
    else if(searchNode != NULL && addNodePtr != NULL)
    {//Center Node
      SETP(PRED_PTR(bp), searchNode);
      SETP(SUCC_PTR(searchNode), bp);
      SETP(SUCC_PTR(bp), addNodePtr);
      SETP(PRED_PTR(addNodePtr), bp);
    }
    return;
}
//Simple remove node to seg list
static void removeNode(void *bp)
{
    int list = 0;
    size_t size = GET_SIZE(HDRP(bp));
    int i;
    for(i = 0; i < (LISTS - 1); i++)
    {
      if(size > 1)
      {
        size = size >> 1;
        list++;
      }
    }
    if (PRED(bp) == NULL && SUCC(bp) == NULL)
    {
      segregated_list[list] = NULL;
    }
    else if(PRED(bp) == NULL && SUCC(bp) != NULL)
    {
      SETP(PRED_PTR(SUCC(bp)), NULL);
    }
    else if(PRED(bp) != NULL && SUCC(bp) == NULL)
    {
      SETP(SUCC_PTR(PRED(bp)), NULL);
      segregated_list[list] = PRED(bp);
    }
    else if(PRED(bp) != NULL && SUCC(bp) != NULL)
    {
      SETP(SUCC_PTR(PRED(bp)), SUCC(bp));
      SETP(PRED_PTR(SUCC(bp)), PRED(bp));
    }
    return;
}
/*
  Used the ideafrom text :
  Place block of asize bytes at start of free block bp
  and split if remainder would be at least minimum block size
*/
static void *place(void *bp, size_t asize)
{
    size_t csize = GET_SIZE(HDRP(bp));
    removeNode(bp);
    if (csize - asize >=  (2*DSIZE))
    {//Split
        PUT(HDRP(bp), PACK(csize - asize, 0));
        PUT(FTRP(bp), PACK(csize - asize, 0));
        PUT_NO_TAG(HDRP(NEXT_BLKP(bp)), PACK(asize, 1));
        PUT_NO_TAG(FTRP(NEXT_BLKP(bp)), PACK(asize, 1));
        addNode((bp), csize - asize);
        return NEXT_BLKP(bp);
    }
    else
    { //Do not split
      PUT(HDRP(bp), PACK(csize, 1));
      PUT(FTRP(bp), PACK(csize, 1));
    }
    return bp;
}

//Text based
static void *extend_heap(size_t words)
{
    void *bp;
    size_t asize;                // Adjusted size
    asize = ALIGN(words);
    if ((bp = mem_sbrk(asize)) == (void *)-1)
        return NULL;

/* Initialize free block header/footer and the epilogue header */
    PUT_NO_TAG(HDRP(bp), PACK(asize, 0));
    PUT_NO_TAG(FTRP(bp), PACK(asize, 0));
    PUT_NO_TAG(HDRP(NEXT_BLKP(bp)), PACK(0, 1));
    addNode(bp, asize);
     /* Coalesce if the previous block was free */
    return coalesce(bp);
}
/*
  Remove which are being coalesced. Boundary tag coalescing
*/
static void *coalesce(void *bp)
{
    size_t prev_alloc = GET_ALLOC(HDRP(PREV_BLKP(bp)));
    size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(bp)));
    size_t size = GET_SIZE(HDRP(bp));
    // Do not coalesce with previous block if the previous block is tagged with Reallocation tag
    if (GET_TAG(HDRP(PREV_BLKP(bp))))
        prev_alloc = 1;

    if (prev_alloc && next_alloc)
    {                   // Case 1
        return bp;
    }
    else if (prev_alloc && !next_alloc)
    {                   // Case 2
        removeNode(bp);
        removeNode(NEXT_BLKP(bp));
        size += GET_SIZE(HDRP(NEXT_BLKP(bp)));
        PUT(HDRP(bp), PACK(size, 0));
        PUT(FTRP(bp), PACK(size, 0));
    }
    else if (!prev_alloc && next_alloc)
    {                 // Case 3
        removeNode(bp);
        removeNode(PREV_BLKP(bp));
        size += GET_SIZE(HDRP(PREV_BLKP(bp)));
        PUT(FTRP(bp), PACK(size, 0));
        PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
        bp = PREV_BLKP(bp);
    }
    else
    {                // Case 4
        removeNode(bp);
        removeNode(PREV_BLKP(bp));
        removeNode(NEXT_BLKP(bp));
        size += GET_SIZE(HDRP(PREV_BLKP(bp))) + GET_SIZE(HDRP(NEXT_BLKP(bp)));
        PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
        PUT(FTRP(NEXT_BLKP(bp)), PACK(size, 0));
        bp = PREV_BLKP(bp);
    }
    addNode(bp, size);
    return bp;
}

/******************************REQUIRED FUNCTIONS********************/
/*
 * mm_init - initialize the malloc package.
 * Before calling mm_malloc, mm_realloc, or mm_free,
 * the application program calls mm_init to perform any necessary initializations,
 * such as allocating the initial heap area.
 *
 * Return value : -1 if there was a problem, 0 otherwise.
 */
int mm_init(void)
{
    int i;
    char *start; // Pointer to beginning of heap

    // Initialize segregated free lists
    for (i = 0; i < LISTS; i++)
    {
        segregated_list[i] = NULL;
    }
    start = mem_sbrk(4 * WSIZE);
    if ((long) start == -1)
        return -1;

    PUT_NO_TAG(start, 0);                           /* Alignment padding */
    PUT_NO_TAG(start + (1 * WSIZE), PACK(DSIZE, 1)); /* Prologue header */
    PUT_NO_TAG(start + (2 * WSIZE), PACK(DSIZE, 1)); /* Prologue footer */
    PUT_NO_TAG(start + (3 * WSIZE), PACK(0, 1));     /* Epilogue header */

    if (extend_heap(START) == NULL)
        return -1;
    return 0;
}
/*
  Allocate a block whose size is muliple of alignment.
*/
void *mm_malloc(size_t size)
{
    size_t asize;      /* Adjusted block size */
    size_t extendsize; /* Amount to extend heap if no fit */
    void *bp = NULL;  /* Pointer */

    if (size == 0)
        return NULL;

    if (size <= DSIZE){  asize = 2 * DSIZE; }
    else
    {
      asize = ALIGN(size+DSIZE);
    }
    int list = 0;
    size_t searchsize = asize;
    //Lookout for free blocks first.
    for(list = 0;list < LISTS;list++)
    {
        if ((list == LISTS - 1) || ((searchsize <= 1)
          && (segregated_list[list] != NULL)))
        {  //Check size and skip the reallocated ones.
            bp = segregated_list[list];
            while ((bp != NULL)&& ((asize > GET_SIZE(HDRP(bp)))
              || (GET_TAG(HDRP(bp)))))
            {
              bp = PRED(bp);
            }
            if (bp != NULL)
                break;
        }
        searchsize >>= 1;
    }
    if (bp == NULL)
    {//Block unavaialable then extend the size of the heap.
      extendsize = MAX(asize, CHUNKSIZE);
      if ((bp = extend_heap(extendsize)) == NULL)
        return NULL;
    }
    bp = place(bp, asize); //Place as per book
    return bp;
}
/*
 * mm_free - Freeing a block does nothing.
 * Role : The mm_free routine frees the block pointed to by ptr
 * Return value : returns nothing (Same a text)
 */
void mm_free(void *bp)
{
    if(bp == 0)
      return;

    size_t size = GET_SIZE(HDRP(bp));

    REMOVE_RATAG(HDRP(NEXT_BLKP(bp)));
    PUT(HDRP(bp), PACK(size, 0));
    PUT(FTRP(bp), PACK(size, 0));
    addNode(bp, size);
    coalesce(bp);
    return;
}
/*
*     Padding with buffer to ensure next reallocation
*     can be done wihtout reallocation.
*     Add tag to reallocated.
*     Remove when heap is extended, freed, used by reallocation.
*/
void *mm_realloc(void *ptr, size_t size)
{
    void *newp = ptr;
    size_t copySize = size;
    if (size == 0)
        return NULL;
    copySize = copySize + REALLOC;
    int remainder;
    //(realloc-bal.rep, realloc2-bal.rep)
    int temp = GET_SIZE(HDRP(NEXT_BLKP(ptr))); //Next block epilogiu?
    int temp2 = GET_ALLOC(HDRP(NEXT_BLKP(ptr)));//Next block free?
    int temp1 = GET_SIZE(HDRP(ptr)) - copySize;
    remainder = GET_SIZE(HDRP(ptr)) + temp - copySize;

    if (temp1 < 0)
    {//Only if not allocated. && check epilogue
      if(temp2 != 1 || temp != 0)
      {
        if (remainder < 0)
        {
          extend_heap(MAX(-remainder, CHUNKSIZE));
          remainder = remainder + MAX(-remainder, CHUNKSIZE);
        }
        removeNode(NEXT_BLKP(ptr));
        PUT_NO_TAG(HDRP(ptr), PACK(copySize + remainder, 1));
        PUT_NO_TAG(FTRP(ptr), PACK(copySize + remainder, 1));
      }
      else
      {
        newp = mm_malloc(copySize - DSIZE);
        memcpy(newp, ptr, MIN(size, copySize));
        mm_free(ptr);
      }
    }
    temp1 = GET_SIZE(HDRP(newp)) - copySize;
    //Buffer not enough for reallocation. Marking RA tag. Won't be used!
    if (temp1 < (2 * REALLOC))
        SET_RATAG(HDRP(NEXT_BLKP(newp)));
    return newp;
  }
