/*
 * mm-implicit.c -  Simple allocator based on implicit free lists,
 *                  first fit placement, and boundary tag cing.
 *
 * Each block has header and footer of the form:
 *
 *      31                     3  2  1  0
 *      -----------------------------------
 *     | s  s  s  s  ... s  s  s  0  0  a/f
 *      -----------------------------------
 *
 * where s are the meaningful size bits and a/f is set
 * iff the block is allocated. The list has the following form:
 *
 * begin                                                          end
 * heap                                                           heap
 *  -----------------------------------------------------------------
 * |  pad   | hdr(8:a) | ftr(8:a) | zero or more usr blks | hdr(8:a) |
 *  -----------------------------------------------------------------
 *          |       prologue      |                       | epilogue |
 *          |         block       |                       | block    |
 *
 * The allocated prologue and epilogue blocks are overhead that
 * eliminate edge conditions during coalescing.
 */
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <memory.h>
#include "mm.h"
#include "memlib.h"

/*********************************************************
 * NOTE TO STUDENTS: Before you do anything else, please
 * provide your team information in the following struct.
 ********************************************************/
team_t team = {
  /* Team name */
  "ChicoState",
  /* First member's full name */
  "Diney Wankhede",
  /* First member's email address */
  "dwankhede@mail.csuchico.edu",
  /* (leave blank) */
  "",
  /* (leave blank) */
  ""
};

/
#define WSIZE       4       /* word size (bytes) */
#define DSIZE       8       /* doubleword size (bytes) */
#define CHUNKSIZE  (1<<12)  /* initial heap size (bytes) */
#define OVERHEAD    8       /* overhead of header and footer (bytes) */
#define INITCHUNKSIZE (1<<6)


static inline int MAX(int x, int y) {
  return x > y ? x : y;
}

static inline int MIN(int x, int y) {
  return x < y ? x : y;
}

//
// Pack a size and allocated bit into a word
// We mask of the "alloc" field to insure only
// the lower bit is used
//
static inline size_t PACK(size_t size, int alloc) {
  return ((size) | (alloc & 0x1));
}

//
// Read and write a word at address p
//
//static inline size_t GET(void *p) { return  *(size_t *)p; }
#define GET(p) (*(unsigned int *)(p))
static inline void PUT( void *p, size_t val)
{
  *((size_t *)p) = val;
}

//
// Read the size and allocated fields from address p
//
#define GET_SIZE(p) (GET(p) & ~0x7)
/*
static inline size_t GET_SIZE( void *p )  {
  return GET(p) & ~0x7;
}*/

static inline int GET_ALLOC( void *p  ) {
  return GET(p) & 0x1;
}

//
// Given block ptr bp, compute address of its header and footer
//
static inline void *HDRP(void *bp) {

  return ( (char *)bp) - WSIZE;
}
static inline void *FTRP(void *bp) {
  return ((char *)(bp) + GET_SIZE(HDRP(bp)) - DSIZE);
}

//
// Given block ptr bp, compute address of next and previous blocks
//
static inline void *NEXT_BLKP(void *bp) {
  return  ((char *)(bp) + GET_SIZE(((char *)(bp) - WSIZE)));
}

static inline void* PREV_BLKP(void *bp){
  return  ((char *)(bp) - GET_SIZE(((char *)(bp) - DSIZE)));
}


//Additional
#define PUT_TAG(bp) (GET(bp) |= 0x2)
#define REMOVE_TAG(bp) (GET(bp) &= ~0x2)
#define GET_TAG(p) (GET(p) & 0x2)
//#define GET_ALLOC_TAG(bp) (GET(bp) & 0x2)
#define PUT_WITH_TAG(p,val) (*(unsigned int *)(p) = (val) | GET_TAG(p))

#define REALLOC (1<<7)

#define SETPTR(p,bp) (*(unsigned int *)(p) = (unsigned int)(bp))

/* rounds up to the nearest multiple of ALIGNMENT */
#define ALIGNMENT 8
#define ALIGN(size) (((size) + (ALIGNMENT-1)) & ~0x7)
#define PREDSEG(bp) (*(char**)(bp))
#define PRED(bp) ((char*)bp)
#define SUCC(bp) ((char*)bp + WSIZE)
#define SUCCSEG(bp) (*(char **)(SUCC(bp)))

/////////////////////////////////////////////////////////////////////////////
//
// Global Variables
//

static char *heap_listp;  /* pointer to first block */
void* segregated_list[5];

//
// function prototypes for internal helper routines
//
static void *extend_heap(size_t words);
static void *place(void *bp, size_t asize);
//static void *find_fit(size_t asize);
static void *coalesce(void *bp);
static void addNode(void* bp,size_t size);
static void removeNode(void* bp);
static void printblock(void *bp);
static void checkblock(void *bp);


static void addNode(void* bp,size_t size)
{
  void* addNodePtr = NULL;
  int i = 0;
  int list_number = 0;
  void* searchPtr = bp;

/*  for(i = 0; i < 5; i++)
  {
    if(size > 1)
    {
      size >>= 1;
      list_number++;
    }
  //  printf("HI" );
}*/
  while ((list_number <  4) && (size > 1)) {
       size >>= 1;
       list_number++;
   }

  searchPtr = segregated_list[list_number];
  while ((searchPtr != NULL) && (size > GET_SIZE(HDRP(searchPtr))))
  {
//    if()
    {
      addNodePtr = searchPtr;
      searchPtr = PRED(searchPtr);
    }
  }

  if(searchPtr == NULL && addNodePtr == NULL)
  {//First Node
    SETPTR(SUCC(bp),NULL);
    SETPTR(PRED(bp),NULL);
    segregated_list[list_number] = bp;
  }
  else if(searchPtr == NULL && addNodePtr != NULL)
  {//At the begining
    SETPTR(PRED(bp), NULL);
    SETPTR(SUCC(bp), addNodePtr);
    SETPTR(PRED(addNodePtr), bp);
  }
  else if(searchPtr != NULL && addNodePtr == NULL)
  {//At the end
    SETPTR(PRED(bp), searchPtr);
    SETPTR(SUCC(searchPtr), bp);
    SETPTR(SUCC(bp), NULL);
    segregated_list[list_number] = bp;
  }
  else if(searchPtr != NULL && addNodePtr != NULL)
  {//Center Node
    SETPTR(PRED(bp), searchPtr);
    SETPTR(SUCC(searchPtr), bp);
    SETPTR(SUCC(bp), addNodePtr);
    SETPTR(PRED(addNodePtr), bp);
  }
  //(*(unsigned int *)(p) = (unsigned int)(ptr))
  return;
}


static void removeNode(void* bp)
{
   printf("HII" );
  int i = 0;
  int list_number = 0;
   size_t size = GET_SIZE(HDRP(bp));

   for(i = 0; i < 5; i++)
   {
     if(size > 1)
     {
       size >>= 1;
       list_number++;
     }
   }
   /*while ((list_number <  4) && (size > 1)) {
       size >>= 1;
       list_number++;
   }*/

   if(PREDSEG(bp) == NULL && SUCCSEG(bp) == NULL)
   { // Only one node remaining
     segregated_list[list_number] = NULL;
   }
   else if(PREDSEG(bp) == NULL && SUCCSEG(bp) != NULL)
   {//First Node/ Start Node
     SETPTR(PRED(SUCCSEG(bp)), NULL);
   }
   else if(PREDSEG(bp) != NULL && SUCCSEG(bp) == NULL)
   {//Last node in the list
     SETPTR(SUCC(PREDSEG(bp)), NULL);
     segregated_list[list_number] = PREDSEG(bp);
   }
   else if(PREDSEG((bp) != NULL && SUCCSEG(bp) != NULL))
   {
     SETPTR(SUCC(PREDSEG(bp)), SUCCSEG(bp));
     SETPTR(PRED(SUCCSEG(bp)), PREDSEG(bp));
   }
   return;
}

//
// mm_init - Initialize the memory manager
//
int mm_init(void)
{

  char* start;
    int i = 0;
    for(i = 0; i < 5; i++)
    {
      segregated_list[i] = NULL;
    }

  //Extend the heap by 4 * WSIZE and get the size.
  start = mem_sbrk(4 * WSIZE);
  if((long)start == -1)
    return -1;

  //Initialize empty heap
  PUT(start, 0);                          /* Alignment padding */
  PUT(start + (1*WSIZE), PACK(DSIZE, 1)); /* Prologue header */
  PUT(start + (2*WSIZE), PACK(DSIZE, 1)); /* Prologue footer */
  PUT(start + (3*WSIZE), PACK(0, 1));     /* Epilogue header */
  //heap_listp += (2*WSIZE);                     //line:vm:mm:endinit

  if(extend_heap(INITCHUNKSIZE) == NULL)
    return -1;

  return 0;
}


//
// extend_heap - Extend heap with free block and return its block pointer
//
static void *extend_heap(size_t words)
{
  void *bp;
  size_t asize;

 /* Allocate an even number of words to maintain alignment */
// size = (words % 2) ? (words+1) * WSIZE : words * WSIZE;
  asize  = ALIGN(words);
  if ((bp = mem_sbrk(asize)) == (void*)-1)
    return NULL;

   /* Initialize free block header/footer and the epilogue header */
   PUT(HDRP(bp), PACK(asize, 0)); /* Free block header */
   PUT(FTRP(bp), PACK(asize, 0)); /* Free block footer */
   PUT(HDRP(NEXT_BLKP(bp)), PACK(0, 1)); /* New epilogue header */
   addNode(bp,asize);

 /* Coalesce if the previous block was free */
   return coalesce(bp);
}


//
// Practice problem 9.8
//
// find_fit - Find a fit for a block with asize bytes
//
/*static void *find_fit(size_t asize)
{
  return NULL;
}
*/
//
// mm_free - Free a block
//
void mm_free(void *bp)
{
  if(bp == 0)
    return;

  size_t size = GET_SIZE(HDRP(bp));

  REMOVE_TAG(HDRP(NEXT_BLKP(bp)));
  PUT(HDRP(bp), PACK(size, 0));
  PUT(FTRP(bp), PACK(size, 0));

  addNode(bp, size);
  coalesce(bp);
  return;
}

//
// coalesce - boundary tag coalescing. Return ptr to coalesced block
//
static void *coalesce(void *bp)
{
  size_t prev_realloc = GET_TAG(HDRP(PREV_BLKP(bp)));
  size_t prev_alloc = GET_ALLOC(HDRP(PREV_BLKP(bp)));
  size_t next_alloc = GET_ALLOC(HDRP(NEXT_BLKP(bp)));
  size_t size = GET_SIZE(HDRP(bp));

  //Skip the prev block if its already been reallocated.
  if(prev_realloc)
    prev_alloc = 1;

  if (prev_alloc && next_alloc)
  { //When prev and next blocks are already allocated, nothing to do
  	return bp;
  }
  else if (prev_alloc && !next_alloc)
  { //When the next block is free, coalesce with next block, both blocks
    //should be removed from the list
    removeNode(bp);
    removeNode(NEXT_BLKP(bp));
    addNode(bp,size);
  	size += GET_SIZE(HDRP(NEXT_BLKP(bp)));
  	PUT(HDRP(bp), PACK(size, 0));
  	PUT(FTRP(bp), PACK(size,0));
  }
  else if (!prev_alloc && next_alloc)
  {  //When the prev block is free, coalesce with prev block, both blocks
    //should be removed from the list
    removeNode(bp);
    removeNode(PREV_BLKP(bp));
    addNode(bp,size);
  	size += GET_SIZE(HDRP(PREV_BLKP(bp)));
  	PUT(FTRP(bp), PACK(size, 0));
  	PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
  	bp = PREV_BLKP(bp);
  }
  else
  { //When the next & prev blocks are free, coalesce all three blocks
    //3 blocks removed from the list
    removeNode(bp);
    removeNode(NEXT_BLKP(bp));
    removeNode(PREV_BLKP(bp));
    addNode(bp,size);
  	size += GET_SIZE(HDRP(PREV_BLKP(bp))) +
  	    GET_SIZE(FTRP(NEXT_BLKP(bp)));
  	PUT(HDRP(PREV_BLKP(bp)), PACK(size, 0));
  	PUT(FTRP(NEXT_BLKP(bp)), PACK(size, 0));
  	bp = PREV_BLKP(bp);
  }

  return bp;
}

//
// mm_malloc - Allocate a block with at least size bytes of payload
//
void *mm_malloc(size_t size)
{

//  if(heap_listp == 0)
//    mm_init();

  size_t asize;
  size_t extendsize;
  void* bp = NULL;

  if(size == 0)
    return NULL;

  if(size <= DSIZE)
  {
    asize = 2 * DSIZE;
  }
  else
  {
    asize = ALIGN(size + DSIZE);
    //asize = DSIZE * ((size + (DSIZE) + (DSIZE-1)) / DSIZE);
  }

  size_t temp = asize;
  int i = 0;
  //int list_number = 0;
  for(i = 0; i < 5; i++)
  {
    if(((segregated_list[i] != NULL) && (temp <= 1)) || (i == 4))
    {
      bp = segregated_list[i];

      while(bp != NULL)
      {
        if((asize > GET_SIZE(HDRP(bp))) || GET_TAG(HDRP(bp)))
          bp = PRED(bp);
      }
      if(bp != NULL)
        break;
    }
    temp >>= 1;
    //list_number++;
  }

  if(bp == NULL)
  {
    extendsize = MAX(asize, CHUNKSIZE);
     if ((bp = extend_heap(extendsize)) == NULL)
      return NULL;
  }

  bp = place(bp, asize);

  return bp;

}

//
//
// Practice problem 9.9
//
// place - Place block of asize bytes at start of free block bp
//         and split if remainder would be at least minimum block size
//
static void *place(void *bp, size_t asize)
{

  size_t csize  = GET_SIZE(HDRP(bp));
  //size_t remainder = csize - asize;

  removeNode(bp);

  if ((csize - asize) > (2*DSIZE))
  {
     if(asize < 100)
     {
       PUT_WITH_TAG(HDRP(bp), PACK(asize, 1));
       PUT_WITH_TAG(FTRP(bp), PACK(asize, 1));
       bp = NEXT_BLKP(bp);
       PUT(HDRP(bp), PACK(csize-asize, 0));
       PUT(FTRP(bp), PACK(csize-asize, 0));
       addNode(bp,(csize - asize));
     }
     else
     {
       PUT_WITH_TAG(HDRP(bp), PACK(csize-asize, 0));
       PUT_WITH_TAG(FTRP(bp), PACK(csize-asize, 0));
       PUT(HDRP(NEXT_BLKP(bp)), PACK(asize, 1));
       PUT(FTRP(NEXT_BLKP(bp)), PACK(asize, 1));
       addNode(bp, csize-asize);
       return NEXT_BLKP(bp);
     }
  }
  else
  {
    PUT_WITH_TAG(HDRP(bp), PACK(csize, 1));
	  PUT_WITH_TAG(FTRP(bp), PACK(csize, 1));
  }
  return bp;
}

//
// mm_realloc -- implemented for you
//
void *mm_realloc(void *ptr, size_t size)
{
  /*void *newp;
  size_t copySize;

  newp = mm_malloc(size);
  if (newp == NULL) {
    printf("ERROR: mm_malloc failed in mm_realloc\n");
    exit(1);
  }
  copySize = GET_SIZE(HDRP(ptr));
  if (size < copySize) {
    copySize = size;
  }
  memcpy(newp, ptr, copySize);
  mm_free(ptr);
  return newp;*/

  size_t copySize = size;
  void* newp = ptr;

  if(size == 0)
    return NULL;

  if(size <= DSIZE)
  {
    size = 2 * DSIZE;
  }
  else
  {
    size = ALIGN(size + DSIZE);
  }

  size = size + REALLOC;

  int block = GET_SIZE(HDRP(ptr)) - size;
  int next_block_size = GET_SIZE(HDRP(NEXT_BLKP(ptr)));
  int remainder, expand;

  if(block < 0)
  {
      if(!GET_ALLOC(HDRP(NEXT_BLKP(ptr))) || !next_block_size)
      {
        remainder = GET_SIZE(HDRP(ptr)) + next_block_size - size;

        if(remainder < 0)
        {
          expand = MAX(-remainder , CHUNKSIZE);
          if((extend_heap(expand)) == NULL)
          {
              return 0;
          }
          remainder = remainder + expand;
        }
        removeNode(NEXT_BLKP(ptr));
        // Do not split block
        PUT(HDRP(ptr), PACK(size + remainder, 1));
        PUT(FTRP(ptr), PACK(size + remainder, 1));
      }
      else
      {
        newp = mm_malloc(size - DSIZE);
        memcpy(newp, ptr, MIN(copySize, size));
        mm_free(ptr);
      }
       block  = GET_SIZE(HDRP(newp)) - size;
  }
  if(block < (2 * REALLOC))
    PUT_TAG(HDRP(NEXT_BLKP(newp)));
  return newp;
}

//
// mm_checkheap - Check the heap for consistency
//
void mm_checkheap(int verbose)
{
  //
  // This provided implementation assumes you're using the structure
  // of the sample solution in the text. If not, omit this code
  // and provide your own mm_checkheap
  //
  void *bp = heap_listp;

  if (verbose) {
    printf("Heap (%p):\n", heap_listp);
  }

  if ((GET_SIZE(HDRP(heap_listp)) != DSIZE) || !GET_ALLOC(HDRP(heap_listp))) {
	printf("Bad prologue header\n");
  }
  checkblock(heap_listp);

  for (bp = heap_listp; GET_SIZE(HDRP(bp)) > 0; bp = NEXT_BLKP(bp)) {
    if (verbose)  {
      printblock(bp);
    }
    checkblock(bp);
  }

  if (verbose) {
    printblock(bp);
  }

  if ((GET_SIZE(HDRP(bp)) != 0) || !(GET_ALLOC(HDRP(bp)))) {
    printf("Bad epilogue header\n");
  }
}

static void printblock(void *bp)
{
  size_t hsize, halloc, fsize, falloc;

  hsize = GET_SIZE(HDRP(bp));
  halloc = GET_ALLOC(HDRP(bp));
  fsize = GET_SIZE(FTRP(bp));
  falloc = GET_ALLOC(FTRP(bp));

  if (hsize == 0) {
    printf("%p: EOL\n", bp);
    return;
  }

  printf("%p: header: [%d:%c] footer: [%d:%c]\n",
	 bp,
	 (int) hsize, (halloc ? 'a' : 'f'),
	 (int) fsize, (falloc ? 'a' : 'f'));
}

static void checkblock(void *bp)
{
  if ((size_t)bp % 8) {
    printf("Error: %p is not doubleword aligned\n", bp);
  }
  if (GET(HDRP(bp)) != GET(FTRP(bp))) {
    printf("Error: header does not match footer\n");
  }
}
