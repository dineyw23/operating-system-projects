//
// Basically, whenever a process receives/sends a message:
// Stop the timer, computer total, max and min time, start timer again and send
// signal to other process.
//
//
// @Author: Diney Wankhede
// @Student ID: dwankhede
// Reference: http://man7.org/tlpi/code/online/diff/pipes/simple_pipe.c.html
//

using namespace std;

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <string>
#include <limits.h>
#include <sys/time.h>

#include "helper-routines.h"

/*Define Global Variables*/

pid_t   childpid;
timeval t1, t2 ,start, end;
int numtests;
double totalTime = 0;
double startTime = 0;
double elapsedTime = 0;
double max_value = 0;
int store = 0;
int break_loop = 0;
double min_value = 0;
char readbuffer[2];
sigset_t sigprocmask2;

void findmin_value()
{
	if(startTime < min_value || min_value == 0)
		min_value = startTime;
}

void findmax_value()
{
	if(startTime > max_value || max_value == 0)
		max_value = startTime;
}

void compute_total_time()
{
	startTime = (end.tv_sec - start.tv_sec) * 1000.0; //sec t0 ms
	startTime = startTime + (end.tv_usec - start.tv_usec) / 1000.0; //us to ms
	totalTime = totalTime + startTime;
}
/*
	Child catches this: When received, stop the timer compute_total_time,
	find if min or max.

	Most imp: Before existing, signal its parent. (IPC)
	Set timer before that.
*/
void sigusr1_handler(int sig)
{//Child catching this
	gettimeofday(&end, NULL);
	compute_total_time();
	findmin_value();
	findmax_value();
	//Start timer for child to parent
	gettimeofday(&start, NULL);
	//Send child to parent
	kill(getppid(),SIGUSR2);
}
/*
	Parent catches this. Same as sig1 handler.
	Additionally, we need to keep track of numtests. If all tests are done,
	just send SIGTSTP to both process.
*/
void sigusr2_handler(int sig)
{//Parent catching this
	//End time
	gettimeofday(&end, NULL);
	compute_total_time();
	findmin_value();
	findmax_value();
	numtests -= 1;
	if(numtests == 0)
	{ //Okay, I'm done with num tests. SIGTSTP BOTH PROCESS
		kill(childpid,SIGTSTP);
		kill(getpid(),SIGTSTP);
	}
	//Start timer for parent to child
	gettimeofday(&start, NULL);
	//Send parent to child
	kill(childpid,SIGUSR1);
}

/*
	This handler should exit the processes. Before that,
	print the required.
*/
void sigstp_handler(int sig)
{
	// stop timer
	gettimeofday(&t2, NULL);
	// compute and print the time in millisec
	elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;    // sec to ms
	elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0; // us to ms

	if (childpid == 0)
	{
		double average = totalTime/store;
		printf("Child's Results for ");
		printf("Signal IPC mechanisms\n");
		printf("Process ID is %d, Group ID is %d\n", getpid(), getgid());
		printf("Round trip times\n");
		printf("Average %f\n", average);
		printf("Maximum %f\n", max_value);
		printf("Minimum %f\n", min_value);
		printf("Elapsed Time %f\n", elapsedTime);
		exit(0);
	}
	else
	{
		int status;
		waitpid(childpid,&status,0);
		double average = totalTime/store;
		printf("Parent's Results for ");
		printf("Signal IPC mechanisms\n");
		printf("Process ID is %d, Group ID is %d\n", getpid(), getgid());
		printf("Round trip times\n");
		printf("Average %f\n", average);
		printf("Maximum %f\n", max_value);
		printf("Minimum %f\n", min_value);
		printf("Elapsed Time %f\n", elapsedTime);
		exit(0);
	}
}

//
// main - The main routine
//

int main(int argc, char **argv){
	//Initialize Constants here
    //variables for Pipe
	int fd1[2],fd2[2], nbytes;
	//byte size messages to be passed through pipes
	char    childmsg[] = "1";
	char    parentmsg[] = "2";
	char    quitmsg[] = "q";

    /*Three Signal Handlers You Might Need
     *
     *I'd recommend using one signal to signal parent from child
     *and a different SIGUSR to signal child from parent
     */
    Signal(SIGUSR1,  sigusr1_handler); //User Defined Signal 1
    Signal(SIGUSR2,  sigusr2_handler); //User Defined Signal 2
    Signal(SIGTSTP, sigstp_handler);

    //Default Value of Num Tests
    numtests = 10000;
    //Determine the number of messages was passed in from command line arguments
    //Replace default numtests w/ the commandline argument if applicable
    if(argc<2)
		{
		printf("Not enough arguments\n");
		exit(0);
		}
		if(argc == 3)
		{
			int temp = atoi(argv[2]);
			if(temp > 0)
			numtests = temp;
		}
	  store = numtests;
    printf("Number of Tests %d\n", numtests);
    // start timer
	gettimeofday(&t1, NULL);
	if(strcmp(argv[1],"-p")==0)
	{
		int x = pipe(fd1);
		if(x < 0)
		{
			printf("Error creating pipe 1");
			exit(1);
		}

		int y = pipe(fd2);
		if(y < 0)
		{
				printf("Error creating pipe 2");
				exit(1);
		}
		//code for benchmarking pipes over numtests
		if((childpid = fork())== -1)//Get rid if error in fork()
		{
			 perror("fork");
			 exit(1);
		}
		else if(childpid == 0) //If Child
		{
				//Child to Parent
				gettimeofday(&start, NULL);
				while(strcmp(quitmsg, readbuffer))
				{
					close(fd1[0]); //Closing read side of  the pipe1 - child
					close(fd2[1]); //Closing write side of pipe2 - parent
					//Pipe2 input end is reading.
					//Data written to WRITE end of the pipe is read
					int numRead = read(fd2[0], readbuffer, sizeof(readbuffer));
					if(numRead < 0)
					{
						printf("Error reading.\n");
						exit(1);
					}
					if(strcmp(parentmsg, readbuffer) == 0)
					{
						gettimeofday(&end, NULL);
						compute_total_time();
						findmin_value();
						findmax_value();
						gettimeofday(&start, NULL);
						write(fd1[1], childmsg, strlen(readbuffer)+1);
					}
			}
		}
		else
		{
			//Parent to child -> This is to be done first?
			gettimeofday(&start,NULL);
			write(fd2[1],parentmsg,strlen(readbuffer)+1);
			for(;numtests > 0;numtests--)
			{
				int numRead2 = read(fd1[0], readbuffer, sizeof(readbuffer));
				if(numRead2 == -1)
				{
					printf("Error reading: parent.\n");
					exit(1);
				}
				if(numRead2)
				{
					close(fd1[1]);
   				close(fd2[0]);
				}
				if(strcmp(readbuffer,childmsg) == 0)
				{ //Message received
					gettimeofday(&end,NULL);
					compute_total_time();
					findmin_value();
					findmax_value();
				  //printf("%f\n",max_value );
					gettimeofday(&start,NULL);
					if(numtests == 1)
						write(fd2[1], quitmsg, strlen(readbuffer)+1);
					else
						write(fd2[1],parentmsg, strlen(readbuffer)+1);
				}
			}
			int status;
			waitpid(childpid,&status,0);
		}
		// stop timer
		gettimeofday(&t2, NULL);
		// compute and print the elapsed time in millisec
		elapsedTime = (t2.tv_sec - t1.tv_sec) * 1000.0;      // sec to ms
		elapsedTime += (t2.tv_usec - t1.tv_usec) / 1000.0;   // us to ms
		double average = totalTime/store;
		if (childpid == 0)
			printf("Child's Results for ");
		else
		{
			printf("Parent's Results for ");
		}
		printf("Pipe IPC mechanisms\n");
		printf("Process ID is %d, Group ID is %d\n", getpid(), getgid());
		printf("Round trip times\n");
		printf("Average %f\n", average);
		printf("Maximum %f\n", max_value);
		printf("Minimum %f\n", min_value);
		printf("Elapsed Time %f\n", elapsedTime);
	}
	else if(strcmp(argv[1],"-s")==0)
	{
		/* Reference: http://stackoverflow.com/questions/13477714/how-to-send-signal-
		sigusr1-and-sigusr2-from-parent-to-children*/
		//code for benchmarking signals over numtests
		//Get rid if error in fork()
		if((childpid = fork()) == -1){perror("fork"); exit(1);}
		else if(childpid == 0)
		{
			max_value = 0;
		  totalTime = 0;
			sigemptyset(&sigprocmask2);
			sigaddset(&sigprocmask2, SIGUSR2);
			sigaddset(&sigprocmask2, SIGTSTP);
			gettimeofday(&start,NULL);
			for(;;) //Bryan's style of While(1)
			{//Wait util we get a message from parent process
				sigsuspend(&sigprocmask2);
			}
		}
		else
		{
			sigemptyset(&sigprocmask2);
			sigaddset(&sigprocmask2, SIGUSR1);
			sigaddset(&sigprocmask2, SIGTSTP);
			max_value = 0;
			int x = numtests;
			gettimeofday(&start,NULL);
			//Start with sending SIGUSR1 to child
			kill(childpid,SIGUSR1);
			for(;x > 0;x--)
			{
				//waiting for signal for numtests times
				 sigsuspend(&sigprocmask2);
			}
			int status;
			//kill(childpid,SIGTSTP);
			waitpid(childpid,&status,0);
		}
	}

}
