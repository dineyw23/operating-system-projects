/*
 *  @Author: Diney Wankhede
 *  @File: multi-lookup.c
 *  @Student-ID: dwankhede
 */

 #include "multi-lookup.h"

int main(int argc, char* argv[])
{
  int no_of_input_files = argc - 2;
  FILE* inputfp[no_of_input_files];
  FILE* outputfp = NULL;
  pthread_mutex_t request_queue_mutex;
  pthread_mutex_t output_mutex;

  char errorstr[SBUFSIZE];
  int return_check;
  int i;
  queue request_queue;

  pthread_t requester_threads[no_of_input_files];
  pthread_t resolver_threads[MAX_RESOLVER_THREADS];
  info request_data[no_of_input_files];
  info resolve_data[MAX_RESOLVER_THREADS];

  /* Check for input*/
  if(argc < MINARGS)
  {
    fprintf(stderr, "Not enough arguments: %d\n", (argc - 1));
    fprintf(stderr, "Usage:\n %s %s\n", argv[0], USAGE);
    return EXIT_FAILURE;
  }

  /*Queue initialization*/
  if((queue_init(&request_queue, QUEUEMAXSIZE)) == QUEUE_FAILURE)
  {
    fprintf(stderr, "Error in queue_init\n");
  }

  return_check = pthread_mutex_init(&request_queue_mutex,NULL);
  if(return_check)
  {
    fprintf(stderr, "Error in mutex_init for request queue. Error: %d \n", return_check);
  }

  return_check = pthread_mutex_init(&output_mutex,NULL);
  if(return_check)
  {
    fprintf(stderr, "Error in mutex_init for output_mutex. Error: %d \n",return_check);
  }

  /* Open Output File */
  outputfp = fopen(argv[(argc-1)], "w");
  if(!outputfp)
  {
    perror("Error Opening Output File");
    return EXIT_FAILURE;
  }

  /* Loop Through Input Files */
  for(i=1; i<(argc-1); i++)
  {
    /* Open Input File */
    inputfp[i - 1] = fopen(argv[i], "r");
    if(!inputfp[i -1])
    {
      sprintf(errorstr, "Error Opening Input File: %s", argv[i]);
      perror(errorstr);
      break;
    }
  }

  for(i = 0; i < no_of_input_files; i++)
  {
      request_data[i].file = inputfp[i];
      request_data[i].queue_mutex = request_queue_mutex;
      request_data[i].domains = &request_queue;
      return_check = pthread_create(&requester_threads[i],
      NULL, requester,&request_data[i]);
      if(return_check)
      {
        fprintf(stderr, "Error in requester creation. Error: %d\n",return_check);
        exit(0);
      }
    }

    for(i = 0; i < MAX_RESOLVER_THREADS; i++)
    {
      resolve_data[i].file = outputfp;
      resolve_data[i].queue_mutex = request_queue_mutex;
      resolve_data[i].file_mutex = output_mutex;
      resolve_data[i].domains = &request_queue;
      return_check = pthread_create(&resolver_threads[i],
      NULL, resolver,&resolve_data[i]);
      if(return_check)
      {
        fprintf(stderr, "Error in resolver creation. Error: %d\n",return_check);
        exit(0);
      }
    }

    for(i = 0; i < no_of_input_files; i++)
    {
      return_check = pthread_join(requester_threads[i], NULL);
      if(return_check)
      {
        fprintf(stderr, "Error: %d in pthread_join of requester \n",return_check);
        exit(0);
      }
    }

    completed_requests = 1;

    for(i = 0; i < MAX_RESOLVER_THREADS; i++)
    {
      return_check = pthread_join(resolver_threads[i], NULL);
      if(return_check)
      {
        fprintf(stderr, "Error: %d in pthread_join of resolver \n",return_check);
        exit(0);
      }
    }

    fclose(outputfp);

    queue_cleanup(&request_queue);

    return_check = pthread_mutex_destroy(&request_queue_mutex);
    if(return_check)
      fprintf(stderr, "Error: %d in queue mutex destroy method.\n",return_check);

    return_check = pthread_mutex_destroy(&output_mutex);
    if(return_check)
      fprintf(stderr, "Error: %d in queue mutex destroy method.\n",return_check);

    return EXIT_SUCCESS;
}

void* requester(void* thread_data)
{
    char hostname[SBUFSIZE];
    char* string;
    int success = 0;
    info* thread_storage = thread_data;
    FILE* inputfp = thread_storage -> file;
    pthread_mutex_t queue_mutex = thread_storage -> queue_mutex;
    queue* store = thread_storage -> domains;
    int return_check;

    while(fscanf(inputfp,INPUTFS,hostname) > 0)
    {
      while(!success)
      {
        /*Lock the queue -> Shared Resource*/
        return_check = pthread_mutex_lock(&queue_mutex);
        if(return_check)
        {
          fprintf(stderr, "Error: %d in the pthread_mutex_lock of queue.\n",return_check);
        }
        /* If queue is full, sleep*/
        if(queue_is_full(store))
        {
          return_check = pthread_mutex_unlock(&queue_mutex);
          if(return_check)
          {
            fprintf(stderr, "Error: %d in the pthread_mutex_unlock of queue.\n",return_check);
          }
          usleep(rand()%100); // pthread-hello.c
        }
        else
        {
          string = malloc(SBUFSIZE);
          strncpy(string,hostname,SBUFSIZE);
          /*Push to the queue */
          if(queue_push(store, string) == QUEUE_FAILURE)
          {
            fprintf(stderr, "Error: Queue Push\n");
          }
          return_check = pthread_mutex_unlock(&queue_mutex);
          if(return_check)
          {
            fprintf(stderr, "Error: %d in the pthread_mutex_unlock of queue.\n",return_check);
          }
          success = 1;
        }
      }
      success = 0;
    }
    fclose(inputfp);
    return EXIT_SUCCESS;
}


void* resolver(void* thread_data)
{
  info* thread_storage = thread_data;
  FILE* file = thread_storage -> file;
  pthread_mutex_t queue_mutex = thread_storage -> queue_mutex;
  pthread_mutex_t file_mutex = thread_storage -> file_mutex;
  queue* store = thread_storage -> domains;
  int return_check;

  char* string;
  char firstipstr[INET6_ADDRSTRLEN];

 /*Untill the queue isn't empty or until all the requests are completed*/
  while(!queue_is_empty(store) || !completed_requests)
  {
    /*Lock the queue -> Shared Resource*/
    return_check = pthread_mutex_lock(&queue_mutex);
    if(return_check)
    {
      fprintf(stderr, "Error: %d in the pthread_mutex_lock of queue.\n",return_check);
    }
    string = queue_pop(store);
    if(string == NULL)
    {
      return_check = pthread_mutex_unlock(&queue_mutex);
      if(return_check)
      {
        fprintf(stderr, "Error: %d in the pthread_mutex_unlock of queue.\n",return_check);
      }
      usleep(rand()%100);
    }
    else
    {
      return_check = pthread_mutex_unlock(&queue_mutex);
      if(return_check)
      {
        fprintf(stderr, "Error: %d in the pthread_mutex_unlock of queue.\n",return_check);
      }

      //Lookup hostname and get IP string
      if(dnslookup(string, firstipstr, sizeof(firstipstr)) == UTIL_FAILURE)
      {
        fprintf(stderr, "dnslookup error: %s\n", string);
        strncpy(firstipstr, "", sizeof(firstipstr));
      }

      /*Lock the output file -> shared resouce */
      return_check = pthread_mutex_lock(&file_mutex);
      if(return_check)
      {
        fprintf(stderr, "Error: %d in the pthread_mutex_lock of output file.\n",return_check);
      }
      fprintf(file,"%s, %s\n",string, firstipstr);

      return_check = pthread_mutex_unlock(&file_mutex);
      if(return_check)
      {
        fprintf(stderr, "Error: %d in the pthread_mutex_unlock of output file.\n",return_check);
      }
      free(string);
    }
  }
  return EXIT_SUCCESS;
}
