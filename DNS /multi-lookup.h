#ifndef MULTI_LOOKUP_H
#define MULTI_LOOKUP_H

#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <pthread.h>

#include "queue.h"
#include "util.h"

#define MIN_ARGS 3
#define USAGE "<inputFilePath> <inputFilePath>...<outputFilePath>"
#define SBUFSIZE 1025
#define INPUTFS "%1024s"
#define MAX_IP_LENGTH INET6_ADDRSTRLEN
#define MAX_INPUT_FILES 10
#define MAX_RESOLVER_THREADS 10
#define MIN_RESOLVER_THREADS 2
#define MAX_NAME_LENGTH 1025
#define QUEUE_SIZE 1024
#define MINARGS 3

struct threads
{
  FILE* file;
  queue *domains;
  pthread_mutex_t queue_mutex;
  pthread_mutex_t file_mutex;
};

typedef struct threads info;

int completed_requests = 0;

void *requester(void* pointer);
void *resolver(void* pointer);

#endif
