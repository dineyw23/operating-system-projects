/*
 *  Name: Diney Wankhede	
 *	Email: dwankhede@mail.csuchico.edu
 *	 
 * 	Proxy will be act middle man,as a server to the web browser.
 *	Open a connection and Accept the request from the client.
 *	The website we visit will be the end server. Requests to the end server via proxy.
 *	*Get back the result from the server and back to browser.
 *	The requests made via the proxy by HTTP should be logged in the file.
 *  
 *  The hints provided provided following info.
 *  - Writes to log file which is protected by unamed semaphore.
 *  - Based on tiny web server from CSAPP book and echo server.
 *  - Error handling is adopted from CSAPP book.
 *  - Written wrappers around Robust I/O to just print warnings.
 *  - Used Pthreads to create threads for multiple connections.
 *  - Calls to thread unsafe functions are wrapped. Description below.
 *  
 */

#include "csapp.h"

/*
 * Golbal variables
*/

sem_t fileSem;
sem_t serverSem;

/*
 * Cannot pass two args to pthread_create function, hence using this.
 */

struct args
{
  int myFd;
  struct sockaddr_in mySock;
};

/*
 * Function prototypes
 */

int parse_uri(char *uri, char *target_addr, char *path, int  *port);
void format_log_entry(char *logstring, struct sockaddr_in *sockaddr, char *uri, int size);
void error(int fd, char *cause, char *errnum, char *shortmsg, char *longmsg);
void *thread(void *);
int readSend(rio_t *rio, char *body, int len, int fd);
void Exit(int fd1, int fd2);

static const char *EMSG = "HTTP/1.1 502 Bad Gateway\r\n";

/*
 *	All Wrappers around Robost I/O to ensure that it does not exit but prints warnings
 *  and returns.
 */

ssize_t Rio_readn_w(int fd, void *ptr, size_t nbytes) 
{
    ssize_t n;
  
    if ((n = rio_readn(fd, ptr, nbytes)) < 0)
    {
			printf("Warning: rio_readn function failed!\n");
			return 0;
		}
		return n;
}

void Rio_writen_w(int fd, void *usrbuf, size_t n) 
{
    if (rio_writen(fd, usrbuf, n) != n)
		{
			printf("Warning: rio_writen function failed!\n");
			return;
		}
}

void Rio_readinitb_w(rio_t *rp, int fd)
{
    rio_readinitb(rp, fd);
} 

ssize_t Rio_readnb_w(rio_t *rp, void *usrbuf, size_t n) 
{
    ssize_t rc;

    if ((rc = rio_readnb(rp, usrbuf, n)) < 0)
		{
			printf("Warning: rio_readnb function failed!\n");
			return 0;	
		}	
	return rc;
}

ssize_t Rio_readlineb_w(rio_t *rp, void *usrbuf, size_t maxlen) 
{
    ssize_t rc;

    if ((rc = rio_readlineb(rp, usrbuf, maxlen)) < 0)
		{
			printf("Warning: rio_readlineb function failed!\n");
			return 0;  
		}
	return rc;
} 

/*
 * Thread safe function wrapper using lock and copy technique. 
 * Apopted from csapp.c and added Lock and copy technique to it. 
 *
 */

int open_clientfd_ts(char *hostname, int port) 
{
    int clientfd;
		struct hostent temp;
    struct hostent *hp = &temp; 
		struct hostent *hp2;
		struct sockaddr_in serveraddr;
		
    if ((clientfd = socket(AF_INET, SOCK_STREAM, 0)) < 0)
			return -1; 

		P(&serverSem);
    if ((hp2 = gethostbyname(hostname)) == NULL)
			return -2; 
		memcpy(hp,hp2,sizeof(struct hostent));
		V(&serverSem);

    bzero((char *) &serveraddr, sizeof(serveraddr));
    serveraddr.sin_family = AF_INET;
    bcopy((char *)hp -> h_addr_list[0], 
	  (char *)&serveraddr.sin_addr.s_addr, hp->h_length);
    serveraddr.sin_port = htons(port);
    if (connect(clientfd, (SA *) &serveraddr, sizeof(serveraddr)) < 0)
			return -1;
    return clientfd;
}

int Open_clientfd_ts(char *hostname, int port) 
{
    int rc;

    if ((rc = open_clientfd_ts(hostname, port)) < 0) 
		{
			if (rc == -1)
				unix_error("Open_clientfd Unix error");
			else        
				dns_error("Open_clientfd DNS error");
    }
    return rc;
}

/*
 * main - Main routine for the proxy program
 */

int main(int argc, char **argv)
{
    //To avoid termincation of the process due to SIGPIPE signal and avoiding
		//proxy to crash.  
		Signal(SIGPIPE, SIG_IGN);
    int connfd, port, listenfd;
		socklen_t clientlen;
		struct sockaddr_in clientaddr;
		pthread_t tid;
	  if(argc == 2) 
			port =	atoi(argv[1]);
		else
		{
			fprintf(stderr, "usage: %s <port>\n", argv[0]); 
			exit(0);
		}
    
		Sem_init(&fileSem, 0, 1); //For log file
    Sem_init(&serverSem, 0, 1);//For the functions

    listenfd = Open_listenfd(port);
    for(;;)
    {
			clientlen = sizeof(struct sockaddr_in);
			struct args *argsp = (struct args*)malloc(sizeof(struct args));
			connfd = Accept(listenfd, (SA *)(&(clientaddr)), &clientlen);
			argsp -> mySock = clientaddr;
			argsp -> myFd = connfd;
			Pthread_create(&tid, NULL, thread, (void *)argsp);
    }
    exit(0);
}



void *thread(void *vargs)
{
    struct args *args1 = (struct args *)vargs;
		//To avoid memory leaks, run as detachable, by default pthreads are joinable.
    Pthread_detach(pthread_self());
    int fd = args1 -> myFd;
    struct sockaddr_in sock;
    memcpy(&sock, &(args1 -> mySock), sizeof(struct sockaddr_in));
		Free(args1);
    char buf[MAXLINE];
    char hostname[MAXLINE], pathname[MAXLINE];
    char line[MAXLINE];
    char method[MAXLINE], uri[MAXLINE], version[MAXLINE];
    rio_t rio;
		FILE *log_file;
    Rio_readinitb_w(&rio, fd);
    Rio_readlineb_w(&rio, buf, MAXLINE);

    sscanf(buf, "%s %s %s", method, uri, version);
    if(strcmp(method, "GET"))
    {
			Rio_writen_w(fd, (void *) EMSG, strlen(EMSG));
			Exit(fd, 0);
			return NULL;
    }
    int port;
    if (parse_uri(uri, hostname, pathname, &port))
    {
        error(fd, method, "400", "Bad Request", "parse_uri error");
        Exit(fd, 0);
        return NULL;
    }

		// After establishing a connection to the web server,
		// the proxy will send complete request to the server,
		//and then enters a loop that repeatedly reads a text line from the end server,
		//and send it back to the browser.

    sprintf(line, "%s %s %s\r\n", method, pathname, version);
    rio_t rio_client;
    int client_fd = Open_clientfd_ts(hostname, port);
   
	  Rio_readinitb_w(&rio_client, client_fd);
    Rio_writen_w(client_fd, line, strlen(line));
   
		 while (Rio_readlineb_w(&rio, line, MAXLINE) > 2)
     {
        if (strstr(line, "Proxy-Connection"))
            strcpy(line, "Proxy-Connection: close\r\n");
        else if (strstr(line, "Connection"))
            strcpy(line, "Connection: close\r\n");

        Rio_writen_w(client_fd, line, strlen(line));

    }

    Rio_writen_w(client_fd, "\r\n", 2);

    int len = 0;
    int flag = 0;

    while (Rio_readlineb_w(&rio_client, buf, MAXLINE) > 2)
    {
        char *result = strstr(buf, "Content-Length");
        if (result)
        {
            flag = 1;
            result += 16;
            len = atoi(result);
        }
			 Rio_writen_w(fd, buf, strlen(buf));
    }

    Rio_writen_w(fd, "\r\n", 2);
    char body[MAXLINE];
    int total_size = 0;
    if (flag != 0)
    {
			total_size = readSend(&rio_client, body, len, fd);
    }
    else
		{
			int size;
			while ((size = Rio_readnb_w(&rio_client, body, MAXLINE)) > 0)
					Rio_writen_w(fd, body, size);
    }

    Exit(client_fd, fd);
		// Okay, make sure to lock the critical section before writing to log file.
		P(&fileSem);
    format_log_entry(buf, &sock, uri, total_size);
		log_file = fopen("./proxy.log", "a");
    fprintf(log_file, "%s\n", buf);
    fflush(log_file);
		fclose(log_file);
		V(&fileSem);
    return NULL;
}

/* 
 *	Reading the request's body and send back to the client.
 *  
 */

int readSend(rio_t *rio, char *body, int length, int fd)
{
    int totalSize = 0;
    int size,readn;
		while (length > 0)
    {
				if(length > MAXLINE)
					readn = MAXLINE;
				else
					readn = length;
				size = Rio_readnb_w(rio, body, readn);
        if(size != readn)
        {
            fprintf(stderr, "Error: Reading from server!\n");
            Exit(rio -> rio_fd, fd);
            exit(0);
        }
        totalSize += size;
        Rio_writen_w(fd, body, size);
        length -= readn;
    }
    return totalSize;
}
/*
 * Just to safe a few line and allow multiple closing of decs.
*/
void Exit(int fd1, int fd2)
{
	if (fd1 > 0)
			Close(fd1);

	if (fd2 > 0)
			Close(fd2);
}

/*
 * Adopted from clienterror from CSAPP.
 * Just to ease printing the error message. If any.
*/

void error(int fd, char *cause, char *errnum,
                 char *shortmsg, char *longmsg)
{
    char buf[MAXLINE], body[MAXBUF];

    /* Build the HTTP response body */
    sprintf(body, "%s: %s\r\n", errnum, shortmsg);
    sprintf(body, "%s%s: %s", body, longmsg, cause);

    /* Print the HTTP response */
    sprintf(buf, "HTTP/1.0 %s %s\r\n", errnum, shortmsg);
    Rio_writen_w(fd, buf, strlen(buf));
    sprintf(buf, "Content-type: text/html\r\n");
    Rio_writen_w(fd, buf, strlen(buf));
    sprintf(buf, "Content-length: %d\r\n\r\n", (int)strlen(body));
    Rio_writen_w(fd, buf, strlen(buf));
    Rio_writen_w(fd, body, strlen(body));
}

/*
 * parse_uri - URI parser
 *
 * Given a URI from an HTTP proxy GET request (i.e., a URL), extract
 * the host name, path name, and port.  The memory for hostname and
 * pathname must already be allocated and should be at least MAXLINE
 * bytes. Return -1 if there are any problems.
 */

int parse_uri(char *uri, char *hostname, char *pathname, int *port)
{
    char *hostbegin;
    char *hostend;
    char *pathbegin;
    int len;

    if (strncasecmp(uri, "http://", 7) != 0) 
		{
			hostname[0] = '\0';
			return -1;
    }
		
    hostbegin = uri + 7;
    hostend = strpbrk(hostbegin, " :/\r\n\0");
    len = hostend - hostbegin;
    strncpy(hostname, hostbegin, len);
    hostname[len] = '\0';

    *port = 80; 
    if (*hostend == ':')
			*port = atoi(hostend + 1);

    pathbegin = strchr(hostbegin, '/');
    if (pathbegin == NULL) 
		{
			pathname[0] = '\0';
    }
    else 
		{
			pathbegin++;
			strcpy(pathname, pathbegin);
    }
	  char tmp[MAXLINE];
    sprintf(tmp, "/%s", pathname);
    strcpy(pathname, tmp);
    return 0;
}

/*
 * format_log_entry - Create a formatted log entry in logstring.
 *
 * The inputs are the socket address of the requesting client
 * (sockaddr), the URI from the request (uri), and the size in bytes
 * of the response from the server (size).
 */

void format_log_entry(char *logstring, struct sockaddr_in *sockaddr,
		      char *uri, int size)
{
    time_t now;
    char time_str[MAXLINE];
    unsigned long host;
    unsigned char a, b, c, d;

    /* Get a formatted time string */
    now = time(NULL);
    strftime(time_str, MAXLINE, "%a %d %b %Y %H:%M:%S %Z", localtime(&now));

    /*
     * Convert the IP address in network byte order to dotted decimal
     * form. Note that we could have used inet_ntoa, but chose not to
     * because inet_ntoa is a Class 3 thread unsafe function that
     * returns a pointer to a static variable (Ch 13, CS:APP).
     */
    host = ntohl(sockaddr -> sin_addr.s_addr);
    a = host >> 24;
    b = (host >> 16) & 0xff;
    c = (host >> 8) & 0xff;
    d = host & 0xff;


    /* Return the formatted log entry string */
    sprintf(logstring, "%s: %d.%d.%d.%d %s %d", time_str, a, b, c, d, uri, size);
}
